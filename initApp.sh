# /usr/bin/bash

###############################################
# Create DockerDB and start it
###############################################
cd /vagrant && docker build -t tellusdb . && cd /vagrant
docker run -d --name tellusdb -p 5432:5432 tellusdb

###############################################
# create env variable for docker DB
###############################################
printf 'export DOCKER_DB_URL="postgres://docker:password@"`docker inspect --format={{.NetworkSettings.IPAddress}} tellusdb`"/docker"'  >> ~/.bashrc

###############################################
# Install local npm libraries
###############################################
cd /vagrant/ui && npm install


###############################################
# Install local grunt
###############################################
cd /vagrant/ui && npm install grunt 

###############################################
# Download activator to  run the application
###############################################
cd /vagrant && wget https://downloads.typesafe.com/typesafe-activator/1.3.6/typesafe-activator-1.3.6-minimal.zip
cd /vagrant && unzip typesafe-activator-1.3.6-minimal.zip
cd /vagrant/activator-1.3.6-minimal/ && mv * ../

###############################################
# Reset bash config settings
###############################################
source ~/.bashrc
