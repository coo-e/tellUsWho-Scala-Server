import play.api.libs.json._

import survey.{JsonType, WrappedWrites}
import survey.constraints._
import survey.dsl._
import survey.questions._
import survey.tojson._

import scalaz._
import Scalaz._

case class Foo(s: String, c: List[Constraint]) extends JsonType {
  val jsonType: String = "foo"
}

val f = Foo("test", List(Min(1)))

val m = Min(1)
m.toJson
val c:Constraint = m
Json.toJson(c)
f.toJson

Json.toJson(survey.sections.Orientation.Horizontal)

val dropItems = Set("item 1", "item 2")
val texte = textEntry("text_1", text = "question text", hint = None)
val drop = dropDown("drop_1", "question text", hint = None)(dropItems)
val drop2 = dropDown("drop_2", "question text", hint = None)(dropItems)
val lik = likert("likert_1", "question text")(answers("one", "two", "three"))
val textWithHint = textEntry("text_2", "question text", hint = "hint text")

texte.toJson
drop.toJson
lik.toJson
textWithHint.toJson

val groups = group("group1") { questions(texte, drop, textWithHint) }
val cgroup = conditionalDeclineGroup("cgroup1", condition = "Decline to answer") { questions(texte, drop) }
Json.prettyPrint(groups.toJson)
Json.prettyPrint(cgroup.toJson)


sealed trait coprodtests
case class test1(i: Int) extends coprodtests
case class test2(s: String) extends coprodtests
case class test3(s: Option[String]) extends coprodtests
val atest: WrappedWrites[coprodtests] = WrappedWrites[coprodtests]
atest.writes(test1(1))
atest.writes(test3("testing".some))
atest.writes(test3(None))
val another: WrappedWrites[BaseQuestion] = WrappedWrites[BaseQuestion]
another.writes(texte)
val msgs = NonEmptyList("one", "two", "three")
msgs.toList.mkString(", ")


val s = section("section_1") { sections(
  subSection("sub_1") { pages (
    page("page_1") { items(texte) }
  )}
)}
Json.prettyPrint(s.asJson)


surveys.backgroundTest.survey.toJson
Json.prettyPrint(surveys.backgroundTest.survey.toJson)

Json.toJson(surveys.infos)