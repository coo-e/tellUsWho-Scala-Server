import survey.constraints._
import scala.annotation.tailrec
import scalaz._
import Scalaz._

trait Id {
  def id: String
}

trait Ids {
  def children: Seq[Id]
}
trait Section extends Id with Ids
trait Page extends Id with Ids
case class S(id: String, children: Seq[Id]) extends Section
case class P(id: String, children: Seq[Q]) extends Section

case class Q(id: String) extends Id


def gatherIds(ids: Traversable[Id]) = {
  @tailrec
  def accIds(acc: Vector[String], ids: Traversable[Id]): Vector[String] = {
    val pages = ids.collect { case p: Page => p }
    val sections = ids.collect { case s: Section => s }

    if(sections.isEmpty) {
      acc ++ pages.map(_.id)
    } else {
      val m = acc ++ pages.map(_.id) ++ sections.map(_.id)
      accIds(m, sections.flatMap(_.children))
    }
  }
  accIds(Vector.empty[String], ids)
}
Vector("test") :+ "testing"

val sections = Seq(S("first",
  Seq(S("second 1", Seq(
    P("page 1", Seq(Q("q1") )),
    P("page 2", Seq(Q("q2")))
  )),
    S("second 2", Seq(P("page 1", Seq(Q("q3")))))
  )
))
val ids = gatherIds(sections)
val badIds = Seq("one", "two", "not good", "+X+")
val r = """\W""".r.unanchored

for {
  id <- badIds
  m <- r findFirstIn id
} yield id




