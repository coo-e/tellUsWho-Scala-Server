package survey

import play.api.libs.json.{JsValue, Json, Writes}

import shapeless.syntax.typeable._

import scala.annotation.tailrec
import scalaz._
import scalaz.std.list._
import scalaz.syntax.traverse._
import scalaz.syntax.validation._

import survey.tojson._

object constraints {

  import scala.language.implicitConversions

  sealed trait Constraint {
    def evaluate(num: Int): Boolean

    def conflictsWith(c: Constraint): Boolean

    def merge(c: Constraint): Constraint
  }

  case object Optional extends Constraint {
    val jsonType = "optional"

    override def evaluate(num: Int): Boolean = true

    override def merge(c: Constraint): Constraint = {
      val o = c.cast[Optional.type].get
      o
    }

    override def conflictsWith(c: Constraint): Boolean = false

    implicit val writes = new Writes[Optional.type] {
      override def writes(o: Optional.type): JsValue = Json.obj(
        "type" -> jsonType
      )
    }
  }

  case object Required extends Constraint {
    val jsonType = "required"

    override def evaluate(num: Int): Boolean = num > 0

    override def merge(c: Constraint): Constraint = {
      val o = c.cast[Required.type].get
      o
    }

    override def conflictsWith(c: Constraint): Boolean = c match {
      case Optional => true
      case _ => false
    }

    implicit val writes = new Writes[Required.type] {
      override def writes(o: Required.type): JsValue = Json.obj(
        "type" -> jsonType
      )
    }
  }

  case class Max(max: Int) extends Constraint with JsonType {
    val jsonType = "max"

    override def evaluate(num: Int): Boolean = if (num < max) true else false

    override def merge(c: Constraint): Constraint = {
      val o = c.cast[Max].get
      if (o.max < max)
        o
      else
        this
    }

    override def conflictsWith(c: Constraint): Boolean = c match {
      case Min(x) => x > max
      case _ => false
    }
  }

  case class Min(min: Int) extends Constraint with JsonType {
    val jsonType = "min"

    override def evaluate(num: Int): Boolean = if (num > min) true else false

    override def merge(c: Constraint): Constraint = {
      val o = c.cast[Min].get
      if (o.min > min)
        o
      else
        this
    }
    override def conflictsWith(c: Constraint): Boolean = c match {
      case Optional => true
      case Max(x) => x < min
      case _ => false
    }
  }

  implicit val constraintWrites = new Writes[Constraint] {
    override def writes(o: Constraint): JsValue = o match {
      case c: Min => c.toJson
      case c: Max => c.toJson
      case c: Required.type => Json.toJson(c)
      case c: Optional.type => Json.toJson(c)
      case _ => ???
    }
  }

  def mergeConstraints[T <: Constraint](constraints: Traversable[T]) = {
    constraints.foldLeft(Map.empty[Class[_], Constraint]) {
      (acc, item) =>
        val current = acc getOrElse(item.getClass, item)
        val merged = item merge current
        acc + (item.getClass -> merged)
    }.values.toVector

  }

 def checkConflicts(constraints: Traversable[Constraint]): ValidationNel[String, List[Constraint]] = {
   def cc(h: Constraint, cs: Traversable[Constraint]): ValidationNel[String, Constraint] = {
     def check(c: Constraint): ValidationNel[String, Constraint]  = {
       if(c.conflictsWith(h))
         s"${h} conflicts with ${c}".failureNel
       else c.successNel
     }
     cs.toList.traverseU(check).fold(e => e.failure, s => h.successNel[String])
   }

   @tailrec
   def rc(l: List[Constraint], v: List[ValidationNel[String, Constraint]]): List[ValidationNel[String, Constraint]] = {
     l match {
       case Nil => "constraints empty".failureNel :: v
       case h :: Nil => h.successNel[String] :: v
       case h :: tail => rc(tail, v :+ cc(h, tail))
     }
   }

   rc(constraints.toList, Nil).sequenceU
 }

  def evaluateConstraints(num: Int, constraints: Traversable[Constraint]) = {
    def check(c: Constraint): ValidationNel[String, Constraint] = {
      if(c.evaluate(num)) {
        c.successNel
      } else {
        s"${num} does not meet constraint requirement ${c}".failureNel
      }
    }
    constraints.toList.traverseU(check)
  }
}