package survey

import shapeless._
import shapeless.labelled._


object coprod {
  trait IsSMember[Root] {
    def apply(s: String): String
  }

  object IsSMember {
    import scala.language.implicitConversions

    def apply[T](implicit d: IsSMember[T]) = d

    implicit def coDescribe[T, C <: Coproduct](
      implicit
      lgen: LabelledGeneric.Aux[T, C],
      c: IsSMemberHelper[T, C]
      ): IsSMember[T] = new IsSMember[T] {
      override def apply(s: String): String = c.describe(s)
    }
  }

  trait IsSMemberHelper[Root, C] {
    def describe(s: String): String
  }

  object IsSMemberHelper {
    import scala.language.implicitConversions

    implicit def cNilFromHList[Root]: IsSMemberHelper[Root, CNil] = new IsSMemberHelper[Root, CNil] {
      override def describe(s: String): String = "CNil"
    }

    implicit def cFromHList[Root, C <: Coproduct, H, Name <: Symbol]
    (implicit
      key: Witness.Aux[Name],
      tpe: Typeable[H],
      cFH: IsSMemberHelper[Root, C]
      ): IsSMemberHelper[Root, FieldType[Name, H] :+: C] =
      new IsSMemberHelper[Root, FieldType[Name, H] :+: C] {
        private[this] val t = tpe
        private[this] val k = key

        def describe(s: String): String = {
          val b = s"searching: ${t.describe} ${t.toString()}, ${k.value}"
          println(b)

          if (s == k.value.toString.stripPrefix("'"))
            s"found: ${t.describe}, ${k}"
          else
            cFH.describe(s)
        }
      }
  }
}
