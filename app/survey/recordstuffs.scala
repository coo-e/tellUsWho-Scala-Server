package survey

import shapeless._
import shapeless.labelled._
import shapeless.ops.hlist.{Align, Selector}
import shapeless.ops.record.{Selector => RecordSelector}
import shapeless.record.Record
import shapeless.tag.Tagged


object recordstuffs {
  trait Field {
    type K
    type V
    type F = FieldType[K, V]
  }

  object Field {
    type Aux[K0, V0, F0] = Field {type K = K0; type V = V0; type F = F0}

    type Tag[T] = Symbol with Tagged[T]

    def apply[T](w: Witness): Aux[Tag[w.T], T, FieldType[Tag[w.T], T]] = new Field {
      type K = Tag[w.T]
      type V = T
    }
  }

  trait FieldsRepr[L <: HList] {
    type Repr <: HList
  }

  object FieldsRepr {
    def apply[L <: HList, K, V](l: F[K, V] :: L)
      (implicit fields: FieldsRepr[F[K, V] :: L]): Aux[F[K, V] :: L, fields.Repr] = fields

    type Aux[L <: HList, Repr0] = FieldsRepr[L] {type Repr = Repr0}
    type F[K0, V0] = Field {type K = K0; type V = V0}

    implicit def fieldsHNil[L <: HNil]: Aux[L, HNil] = new FieldsRepr[L] {
      override type Repr = HNil
    }

    implicit def fieldsHList[L <: HList, K, V]
    (implicit frepr: FieldsRepr[L]): Aux[F[K, V] :: L, FieldType[K, V] :: frepr.Repr] =
      new FieldsRepr[F[K, V] :: L] {
        override type Repr = FieldType[K, V] :: frepr.Repr
      }
  }


  trait BaseQ extends Dynamic {

    import scala.language.dynamics

    type L <: HList
    type Repr <: HList

    type FT[T, V] = FieldType[Symbol with shapeless.tag.Tagged[T], V]
    type Tag[T] = Symbol with Tagged[T]

    def bc: BaseQBuilder[Repr]

    def list: L

    def applyDynamic[T](key: Witness)(value: T)
      (implicit selector: Selector.Aux[Repr, FT[key.T, T]]) = bc(field[Tag[key.T]](value) :: list)

    def selectDynamic(key: Witness)(implicit selector: RecordSelector[L, Tag[key.T]]): selector.Out = selector(list)

    def isComplete(implicit align: Align[L, Repr]) = true
    def complete(implicit align: Align[L, Repr]) = this
  }

  type BQA[Repr0 <: HList, L0 <: HList] = BaseQ { type Repr = Repr0; type L = L0 }

  trait BaseQBuilder[Repr0 <: HList] {
    type BaseQAux[Repr1, L0] = BaseQ {type L = L0; type Repr = Repr1}

    def apply[L <: HList](l: L): BaseQAux[Repr0, L]
    def apply(): BaseQAux[Repr0, HNil]
  }

  object BaseQBuilder {
    def apply[Repr0 <: HList]: BaseQBuilder[Repr0] = new BaseQBuilder[Repr0] {
      val me = this;

      override def apply(): BaseQAux[Repr0, HNil] = apply(HNil)

      override def apply[L0 <: HList](l: L0) = new BaseQ {
        type L = L0
        type Repr = Repr0

        def list: L = l

        def bc: BaseQBuilder[Repr0] = me
      }
    }
  }

  object BaseQ {
    val fields = Field[Int]("id") :: Field[String]("text") :: HNil
    val fieldsRepr = FieldsRepr(fields)
    type Repr = Record.`'id -> Int, 'text -> String`.T

    type R = BaseQ { type Repr = fieldsRepr.Repr}

    val qb: BaseQBuilder[Repr] = BaseQBuilder[Repr]

    def apply() = qb.apply()
  }

  object NextQ {
    val fields = Field[String]("extra") :: BaseQ.fields
    val fieldsRepr = FieldsRepr(fields)

    type R = BaseQ {type Repr = fieldsRepr.Repr}
    val qb: BaseQBuilder[fieldsRepr.Repr] = BaseQBuilder[fieldsRepr.Repr]

    def apply(): R = qb.apply()
  }
}
