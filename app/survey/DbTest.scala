package survey

/**
 * Created by kinghenry on 5/30/15.
 */

import anorm._
import play.api.Play.current
import play.api.db.DB
import play.api.libs.json._
import anorm.SqlParser.{str,int}

class DbTest {

//  def saveRecord(firstName:String ,lastName:String,email:String,sAuth:String) = {
//    DB.withConnection {
//      implicit c =>
//        val id = SQL("insert into survey.users(firstname,lastname,email,sessauth) values ({firstN},{lastN},{eMail},{sessauth})")
//          .on('firstN -> s"$firstName", 'lastN -> s"$lastName", 'eMail -> s"$email", 'sessauth -> s"$sAuth")
//          .executeInsert(SqlParser.scalar[String].singleOpt)
//    }
//  }


//  def retrieveUsers() = {
//    DB.withConnection{
//      implicit c =>
//        val selectUsers = SQL("Select * from users")
//        val results = selectUsers().map(row =>
//        row[String]("firstname") -> row[String]("lastname")
//        ).toList
//    }
//  }

  def upsertUserToken(email:String,sAuth:String) = {
    DB.withConnection  {
      implicit c =>
      val id = SQL(s"With upsert as (UPDATE users set sessauth={sessauth} where email ={email} RETURNING *)"+
        s"INSERT INTO users (email,sessauth) select {email},{sessauth} " +
        s"WHERE NOT EXISTS (SELECT * from upsert)")
      .on('sessauth -> s"$sAuth",'email ->s"$email")
      .executeInsert()
    }
  }



//  def saveResponse(inputJson:JsObject, currentUserEmail:String) = {
//    val questionId = (inputJson \ "questionId").toString().toInt
//    val questionResponse = (inputJson \ "questionId" \ "questionResponse").toString()
//    DB.withConnection {
//      implicit c =>
//        val id = SQL(s"INSERT INTO responses(response,useremail) values ({userResp,eMail})")
//          .on('userResp -> s"$questionResponse", 'eMail -> s"$currentUserEmail")
//          .executeInsert(SqlParser.scalar[String].singleOpt)
//    }
//  }

    def saveResponse(qId:Int,userEmail:String, response:String) = {
      DB.withConnection{
        implicit c =>
          val notAString = qId.toInt
          val id = SQL(s"INSERT INTO responses(response,useremail,questionid) values ({userResp},{eMail},{quesId})")
            .on('userResp -> s"$response", 'eMail -> s"$userEmail",'quesId -> qId)
            .executeInsert(SqlParser.scalar[String].singleOpt)

      }
    }






}
