package survey

import play.api.libs.json._
import shapeless._
import shapeless.labelled.FieldType
import shapeless.ops.hlist.{Mapper, ToTraversable}
import shapeless.ops.product.ToRecord
import shapeless.ops.record.Fields
import shapeless.poly._
import shapeless.record._
import shapeless.syntax.std.product._
import shapeless.syntax.typeable._


final class ProductToJsonOps[P <: Product](p: P) {
  def toJson(implicit toJson: ProductToJson[P]): toJson.Out = toJson(p)
}

trait ProductToJson[T <: Product] extends DepFn1[T] {
  type Out = JsObject
}

trait CoproductWrite[P] {
  type Repr <: Coproduct
  def write(v: Repr): JsObject
}

object CoproductWrite {
  type Aux[P, R] = CoproductWrite[P] { type Repr = R }

  def apply[T](implicit cw: Lazy[CoproductWrite[T]]) = cw


  implicit def cNilWrite[P]: Aux[P, CNil] = new CoproductWrite[P] {
    type Repr = CNil
    override def write(v: CNil): JsObject = ???
  }

  implicit def coproductWrite[P, SubRepr <: Coproduct, Key <: Symbol, Instance <: Product]
  (implicit
    key: Witness.Aux[Key],
    cw: Lazy[Aux[P, SubRepr]],
    ptojson: Lazy[ProductToJson[Instance]]
    ): Aux[P, FieldType[Key, Instance] :+: SubRepr] = new CoproductWrite[P] {
      type Repr = FieldType[Key, Instance] :+: SubRepr

    override def write(v: FieldType[Key, Instance] :+: SubRepr): JsObject = v match {
      case Inl(l) => ptojson.value(l)
      case Inr(r) => cw.value.write(r)
    }
  }
}

trait WrappedWrites[P] extends Writes[P] {
  type Repr <: Coproduct
}

object WrappedWrites {
  type Aux[P, R <: Coproduct] = WrappedWrites[P] { type Repr = R }

  def apply[T](implicit ww: WrappedWrites[T]) = ww

  implicit def create[T, Repr0 <: Coproduct](implicit lgen: LabelledGeneric.Aux[T, Repr0], cw: CoproductWrite.Aux[T, Repr0]): Aux[T, Repr0] = new WrappedWrites[T] {
    type Repr = Repr0
    override def writes(o: T): JsValue = cw.write(lgen.to(o))
  }
}



trait JsonType {
  def jsonType: String
}

object ProductToJson {
  import scala.language.implicitConversions

  def apply[T <: Product](implicit toJson: ProductToJson[T]) = toJson

  object toJsonPoly extends Poly1 {
    implicit def default[T : Writes] = at[T](v => Json.toJson(v))
  }

  object withFieldsToJson extends Poly1 {
    implicit def caseTuple[K <: Symbol, V : Writes] = at[(K, V)] {
      t =>
        val (k, v) = t
        k.toString.stripPrefix("'") -> toJsonPoly(v)
    }
  }

  implicit def createToJson[T <: Product, L <: HList, F <: HList, OutL <: HList]
    (implicit
      toRecord: ToRecord.Aux[T, L],
      fields: Fields.Aux[L, F],
      mapper: Mapper.Aux[withFieldsToJson.type, F, OutL],
      toList: ToTraversable.Aux[OutL, List, (String, JsValue)]
      ) = new ProductToJson[T] {
          override def apply(t: T): Out = {
            val o = JsObject(toList(t.toRecord.fields.map(withFieldsToJson)))
            t.cast[JsonType] match {
              case Some(j) => o + ("type" -> JsString(j.jsonType))
              case None => o
            }
          }
      }
}

