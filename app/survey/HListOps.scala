package survey

import shapeless._
import shapeless.ops.hlist.Align

final class HListToCaseClassOps[L <: HList](l: L) {
  def toCaseClass[T](implicit hlistTo: HListToCaseClass[L, T]) = hlistTo(l)
}

trait ToHListToCaseClassOps {
  import scala.language.implicitConversions
  implicit def toCaseClassOp[L <: HList](l: L) = new HListToCaseClassOps[L](l)
}

trait HListToCaseClass[L <: HList, T] {
  type TRepr <: HList
  def apply(l: L): T
}

object HListToCaseClass {
  type Aux[L <: HList, T, TRepr0 <: HList] = HListToCaseClass[L, T] { type TRepr = TRepr0}

  def apply[L <: HList, T](implicit hlistTo: HListToCaseClass[L, T]): Aux[L, T, hlistTo.TRepr] = hlistTo

  implicit def createHListTo[L <: HList, T, TRepr0 <: HList](implicit
      genT: LabelledGeneric.Aux[T, TRepr0],
      align: Align[L, TRepr0]
    ): Aux[L, T, TRepr0] = new HListToCaseClass[L, T] {
      type TRepr = TRepr0
      def apply(l: L) = genT.from(align(l))
  }
}
