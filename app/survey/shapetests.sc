import shapeless._
import shapeless.labelled._
import shapeless.ops.hlist._
import shapeless.ops.record.{Merger, Keys}
import shapeless.record._
import shapeless.syntax.singleton._
import shapeless.tag._
import shapeless.syntax.std.function._
import survey.builders._
import survey.constraints._
import scala.reflect.runtime.universe.typeOf

val fList = ('number ->> 1) :: ('name ->> "test") :: HNil
case class Foo(name: String, number: Int)
val fooGen = Generic[Foo]
val fooLGen = LabelledGeneric[Foo]
val aFoo = Foo("hi", 2)
val aFooList = fooLGen.to(aFoo)
val cl = Min(1) :: Min(2) :: Max(1) :: Max(2) :: HNil
val keys = Keys[fooLGen.Repr].apply()
def test[L <: HList](l: L, key: String)(implicit selector: Selector[L, Symbol @@ key.type]): selector.Out = selector(l)
test(keys, "name")
def fieldSymbolWithValue[V](key: Witness, value: V) = field[Symbol with Tagged[key.T]](value)
val gened = fieldSymbolWithValue("name", "t") :: fieldSymbolWithValue("number", 2) :: HNil
fooLGen.from(gened)
val tb = Builder[Foo]()
val tb2 = tb name "test" number 1

val otb = Builder[Foo]() name "test" number 1

val apossibleFoo: Foo = tb2.build

def needFoo(f: Foo) = f

needFoo(tb2.build)

needFoo(tb2)

class Test {
  def add = this
  def name(s: String) = this
  def other(n: Int) = this
}
type RI = Record.`'i -> Int`.T
type RS = Record.`'s -> String`.T

val merger = Merger[RI, RS]

type RIS = merger.Out

typeOf[RIS]

val dRI = 'i ->> 1 :: HNil
def typed[T](t: => T) = true

typed[RI](dRI)

val dRIS = 'i ->> 1 :: 's ->> "test" :: HNil
typed[RIS](dRIS)


def testing(s: String, i: Int) = (s, i)

val tP = (testing _).toProduct
