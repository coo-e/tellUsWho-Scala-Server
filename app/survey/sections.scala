package survey

import play.api.libs.json.{Writes, JsValue}
import survey.questions._
import scala.annotation.tailrec
import scala.collection.Traversable
import scalaz._
import Scalaz._

trait SurveyElement {
  def surveyId: String

  def `type`: String

  def asJson: JsValue
}

object sections {

  def checkDuplicateIds(ids: Traversable[String]): ValidationNel[String, Boolean] = {
    val counts = ids.foldLeft(Map.empty[String, Int]) {
      (map, id) =>
        val count = map.getOrElse(id, 0) + 1
        map + (id -> count)
    }
    val duplicates = counts.filter { kv => kv._2 > 1 }.keys.toList
    val messages = duplicates.map { id => s"duplicate id: ${id}" }

    messages.toNel match {
      case Some(l) => l.failure[Boolean]
      case None => true.successNel[String]
    }
  }

  def gatherIds(elements: Traversable[SurveyElement]): List[String] = {
    @tailrec def accIds(acc: List[String], c: Traversable[SurveyElement]): List[String] = {
      val containers = c.collect { case e: SurveyContainer => e }.toList
      val questions = c.collect { case e: BaseQuestion => e }.toList

      if(containers.isEmpty) {
        questions.map(_.surveyId) ::: acc
      } else {
        val m = questions.map(_.surveyId) ::: containers.map(_.surveyId) ::: acc
        accIds(m, containers.flatMap(_.children))
      }
    }

    accIds(List.empty[String], elements)
  }

  trait HasQuestions {
    def questions: Seq[BaseQuestion]

    def questionIds = questions.map(_.surveyId)
  }

  object HasQuestions {
    def unapply(i: HasQuestions): Option[Seq[BaseQuestion]] = i.questions.some
  }

  sealed trait SurveyContainer extends SurveyElement with HasQuestions {
    def children: Seq[SurveyElement]

    def questions: Seq[BaseQuestion] = children.collect { case e: HasQuestions => e }.flatMap(_.questions)
  }

  trait WithName {
    def name: Option[String]
  }

  trait WithHeader {
    def header: Option[String]
  }

  trait Section extends SurveyContainer

  object SurveyContainer {
    implicit val writes: Writes[SurveyContainer] = new Writes[SurveyContainer] {
      override def writes(o: SurveyContainer): JsValue = o.asJson
    }
  }

  trait Page extends SurveyContainer {
    override def questions: Seq[BaseQuestion] = children.collect { case e: HasQuestions => e}.flatMap(_.questions) ++ children.collect{ case q: BaseQuestion => q}
  }

  trait PageElement extends SurveyElement

  object PageElement {
    implicit val writes: Writes[PageElement] = new Writes[PageElement] {
      override def writes(o: PageElement): JsValue = o.asJson
    }
  }

  case class SimpleSection(`type`: String = "section", surveyId: String, children: Seq[SurveyContainer],
                           name: Option[String]) extends Section with WithName {

    import survey.tojson._

    def asJson = this.toJson
  }

  case class SimplePage(`type`: String = "page", surveyId: String, children: Seq[PageElement],
                        name: Option[String]) extends Page with WithName {

    import survey.tojson._

    def asJson = this.toJson
  }

  case class QuestionGroup(`type`: String, surveyId: String, children: Seq[PageElement], header: Option[String],
                           orientation: Orientation) extends PageElement with WithHeader with SurveyContainer {

    import survey.tojson._

    def asJson = this.toJson

    override def questions: Seq[BaseQuestion] = children.collect { case e: HasQuestions => e}.flatMap(_.questions) ++ children.collect{ case q: BaseQuestion => q}
  }

  case class ConditionalQuestionGroup(`type`: String, surveyId: String, condition: String,
                                      children: Seq[BaseQuestion], header: Option[String],
                                      orientation: Orientation) extends PageElement with WithHeader with SurveyContainer {

    import survey.tojson._

    def asJson = this.toJson

    override def questions: Seq[BaseQuestion] = children
  }

  sealed trait Orientation extends JsonType {
    def value: String

    override def jsonType: String = "orientation"
  }

  object Orientation {

    private case class o(value: String) extends Orientation

    val Horizontal: Orientation = o("horizontal")
    val Vertical: Orientation = o("vertical")

    implicit val writes: Writes[Orientation] = WrappedWrites[Orientation]
  }


  case class Survey(`type`: String, surveyId: String, name: String, description: String,
                    children: Seq[SurveyContainer]) extends SurveyElement with HasQuestions {

    import survey.tojson._

    lazy val questions: Seq[BaseQuestion] = children.collect { case e: HasQuestions => e }.flatMap(_.questions)

    def asJson = this.toJson

    lazy val questionsMap: Map[String, BaseQuestion] = questions.foldLeft(Map.empty[String, BaseQuestion]) { (acc, i) => acc + (i.surveyId -> i) }
  }
}
