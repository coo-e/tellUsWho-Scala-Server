import shapeless.HList

package object survey {
  import scala.language.implicitConversions

  implicit def hListToCaseClassOps[L <: HList](l: L) = new HListToCaseClassOps[L](l)

  object tojson {
    implicit def toJsonOps[T <: Product](p: T) = new ProductToJsonOps[T](p)
  }
}
