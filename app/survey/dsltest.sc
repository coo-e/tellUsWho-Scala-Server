import survey.sections.HasQuestions

import scalaz._
import Scalaz._

import survey.constraints._
import survey.constraints.Optional
import survey.dsl._


// constraints
val failConstraints: Traversable[Constraint] = Seq(Min(3), Optional, Required, Max(1))
val successConstraints: Traversable[Constraint] = Seq(Min(1), Max(3), Required)

checkConflicts(failConstraints)
checkConflicts(successConstraints)

// ids

checkValidId("bad id")
checkValidId("+badId+")
checkValidId("goodId")
checkValidId("GoodId")

val badIds = Seq("an id", "+X+")
val goodIds = Seq("id1", "id2", "id_3")

val f = badIds.map(checkValidId)
val success = goodIds.map(checkValidId)

// questions

val dropItems = Set("item 1", "item 2")
val texte = textEntry("text_1", text = "question text", hint = None)
val drop = dropDown("drop_1", "question text", hint = None)(dropItems)
val drop2 = dropDown("drop_2", "question text", hint = None)(dropItems)
val lik = likert("likert_1", "question text")(answers("one", "two", "three"))
val comboSingle = singleComboSearch("combo1", "combo question", hint = "choose one")(dropItems)
val comboMulti = multiComboSearch("combo2", "combo question multi", hint = "choose one or more")(dropItems)

// likert scale answer sequence must be odd
try {
  val likFail = likert("likert_fail", "question text")(answers("one", "two"))
} catch {
  case e: Exception => e.getMessage
}

// combo single can only have Max(x = 1)
try {
  val comboSingleFail = singleComboSearch("combo", "combo question", hint = "choose one", constraints = Max(3))(dropItems)
  println("pass") // passes because merge merges Max(3) into Max(1)
} catch {
  case e: Exception => e.getMessage
}

val textWithHint = textEntry("text_2", "question text", hint = "hint text")

// sections
val s = section("section_1") { sections(
  subSection("sub_1", name = "sub 1") { pages(
  page("page_1", name = "page 1") {items(texte)}
  )}
)}

val groups = group("group1") { questions(texte, drop, textWithHint) }
val group2 = group("group2") { questions(texte, drop, textWithHint) }
val condtionalGroup = conditionalDeclineGroup("group2", condition = "Decline to answer") { questions(texte, drop, textWithHint) }

// check duplicate ids
try {
  val dupId = section("section_1") { sections(
    subSection("sub_1") { pages(
      page("page_1") { items(texte) },
      page("page_1") { items(drop) }
    )}
  )}
} catch {
  case e: Exception => e.getMessage
}

try {
  println("duplicate in page")
  val dupQId = section("section_1") { sections(
    subSection("sub_1") { pages(
      page("page_1") { items(texte, texte) }
    ) }
  )}
} catch {
  case e: Exception => e.getMessage
}

try {
  println("duplicate in separate sections")
  val dupQIdSeparate = section("section1") {
    sections (
      subSection("sub1") {
        pages(
               page("page1") {
                 items(texte)
               },
               page("page2") {
                 items(texte)
               }
         )
      }
    )
  }
} catch {
  case e: Exception => e.getMessage
}

// check full survey
surveys.backgroundTest.survey.questions
surveys.backgroundTest.survey.questionIds
surveys.backgroundTest.survey.questionsMap
surveys.backgroundTest.survey.questionsMap.get("birthDate")
surveys.infos

