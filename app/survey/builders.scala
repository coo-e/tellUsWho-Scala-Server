package survey

import shapeless.labelled._
import shapeless.ops.hlist.{Align, Selector}
import shapeless.tag.Tagged
import shapeless.{HList, HNil, LabelledGeneric, Witness}

object builders {
  import scala.language.dynamics

  trait Builder extends Dynamic {
    type P <: Product
    type L <: HList
    type FT[T, V] = FieldType[Symbol with shapeless.tag.Tagged[T], V]
    type Tag[T] = Symbol with Tagged[T]

    def list: L
    def bc: BuilderObject[P]

    def applyDynamic[T, PRepr <: HList](key: Witness)(value: T)(implicit lgen: LabelledGeneric.Aux[P, PRepr], selector: Selector.Aux[PRepr, FT[key.T, T]]) = bc.apply(field[Tag[key.T]](value) :: list)
    def build[PRepr <: HList](implicit lgen: LabelledGeneric.Aux[P, PRepr], align: Align[L, PRepr]) = lgen.from(align(list))
  }

  /**
   * allows you to create a builder for a case class
   *
   * case class Foo(name: String, number: Int)
   *
   * val fooB = Builder[Foo]
   *
   * val builderFoo = fooB() name "frank" number 2
   *
   * val aFoo: Foo = builderFoo
   *
   * Results:
   *
   * defined class Foo
   * fooB: BuilderObject[Foo] = .builders$Builder$$anon$2@3122b117
   * buildFoo: builders.Builder{type P = Foo; type L = shapeless.::[Int with shapeless.labelled.KeyTag[Symbol with shapeless.tag.Tagged[String("number")],Int],shapeless.::[String with shapeless.labelled.KeyTag[Symbol with shapeless.tag.Tagged[String("name")],String],shapeless.HNil]]} = builders$BuilderObject$$anon$1@23d0944b
   * aFoo: Foo = Foo(frank,2)
   */
  object Builder {
    import scala.language.implicitConversions

    type Aux[P0 <: Product, L0 <: HList] = Builder { type P = P0; type L = L0}

    def apply[P <: Product]: BuilderObject[P] = new BuilderObject[P] {}

    implicit def toP[P <: Product, L <: HList, PRepr <: HList](b: Aux[P, L])(implicit lgen: LabelledGeneric.Aux[P, PRepr], align: Align[L, PRepr]): P = b.build
  }

  trait BuilderObject[P0 <: Product] {
    def apply(): Aux[P0, HNil] = apply(HNil)

    type Aux[P1, L0] = Builder { type P = P1; type L = L0}

    def apply[L0 <: HList](l: L0): Aux[P0, L0] = new Builder {
      type P = P0
      type L = L0
      override def list = l
      override def bc: BuilderObject[P] = BuilderObject.this
    }
  }
}
