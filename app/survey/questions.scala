package survey

import play.api.libs.json.{Writes}
import survey.constraints._
import survey.sections.PageElement

import scala.collection.Traversable

object questions {

  import scala.language.implicitConversions

  trait QuestionMethod

  // Section / SubSection / Page

  // Text Input
  // Drop down
  // Drop down with checkbox to indicate something else
  // Binary choice (yes / no)
  // Question Group with header then questions without text
  // Subtext
  // Question group with header
  // Question group with question that shows new questions
  // Likert scale questions
  // Build your sentence questions
  // Interest question

  sealed trait BaseQuestion extends PageElement {
    def asJson = BaseQuestion.writes.writes(this)
  }

  trait QuestionText {
    self: BaseQuestion =>
    def text: Option[String]
  }

  object QuestionText {
    def unapply(s: QuestionText): Option[String] = s.text
  }

  trait HasConstraints {
    self: BaseQuestion =>
    def constraints: Traversable[Constraint]
  }

  object HasConstraints {
    val empty = List.empty[Constraint]

    def unapply(s: HasConstraints): Option[Traversable[Constraint]] = Some(s.constraints)
  }

  trait LikertScale {
    this: HasConstraints with ClosedAnswerSet =>
  }

  trait ClosedAnswerSet {
    this: BaseQuestion =>
    def answers: Traversable[String]
  }

  trait HintText {
    self: BaseQuestion =>
    def hintText: Option[String]
  }

  trait SecondaryText {
    self: BaseQuestion =>
    def secondaryText: Option[String]
  }

  object ClosedAnswerSet {
    def unapply(s: ClosedAnswerSet): Option[Traversable[String]] = Some(s.answers)
  }

  case class SimpleQuestion(val `type`: String, val surveyId: String, val text: Option[String],
                            val constraints: Traversable[Constraint], val hintText: Option[String],
                            val secondaryText: Option[String])
    extends BaseQuestion with HasConstraints with QuestionText with HintText with SecondaryText

  case class SelectOption(val `type`: String, val surveyId: String, val text: Option[String], val constraints: Traversable[Constraint]) extends BaseQuestion with HasConstraints with QuestionText

  case class LikertQuestion(val `type`: String, surveyId: String, text: Option[String], answers: Seq[String],
                            val constraints: Traversable[Constraint])
    extends BaseQuestion with ClosedAnswerSet with HasConstraints with LikertScale

  case class ClosedAnswerSetQuestion(val `type`: String, val surveyId: String, val text: Option[String],
                                     val answers: Set[String], val constraints: Traversable[Constraint],
                                     hintText: Option[String], val secondaryText: Option[String])
    extends BaseQuestion with ClosedAnswerSet with HasConstraints with HintText with SecondaryText


  object BaseQuestion {
    def unapply(s: BaseQuestion): Option[(String, String)] = Some((s.surveyId, s.`type`))

    //    implicit val writes = new Writes[BaseQuestion] {
    //      override def writes(q: BaseQuestion): JsValue = q match {
    //        case q: SimpleQuestion => q.toJson
    //        case q: LikertQuestion => q.toJson
    //        case q: ClosedAnswerSetQuestion => q.toJson
    //        case _ => ???
    //      }
    //    }

    // in order for this to work the object needs to be places after all the definitions of the classes extend the base trait
    val writes: Writes[BaseQuestion] = WrappedWrites[BaseQuestion]
  }
}
