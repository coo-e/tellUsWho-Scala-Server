package survey

import survey.constraints._
import survey.constraints.Optional
import survey.questions._
import survey.sections._

import scala.collection.Traversable
import scalaz._
import scalaz.Scalaz._

/**
 * This is the dsl used to create surveys
 *
 * import survey.dsl._
 * import survey.constraints._
 */
object dsl {

  import scala.language.implicitConversions
  import ThrowIfFailureOps._

  implicit def singleConstraintToTraversable(c: Constraint): Traversable[Constraint] = Vector(c)

  implicit def oToOption[T](o: T): Option[T] = o.some

  def checkValidId(id: String): Validation[String, String] = {
    val r = """\W""".r.unanchored

    id match {
      case r() => s"invalid id: '${id}'".failure[String]
      case _ => id.success[String]
    }
  }

  def checkIdConstraints(id: String, constraints: Traversable[Constraint]) = {
    (checkValidId(id).toValidationNel |@| checkConflicts(constraints)) {
      (_, _)
    }
  }

  @inline private def requireMax1(constraints: Traversable[Constraint]): Unit = {
    require(constraints.toVector.contains(Max(1)), s"constraints must contain Max(1): ${constraints}")
  }

  trait ConstraintCreator {
    def apply(answers: Int,
              provided: Traversable[Constraint]): Traversable[Constraint] = mergeConstraints((provided ++ compute(answers))
                                                                                             .toIterable)

    def compute(answers: Int): Traversable[Constraint]
  }

  val likertQuestionCC = new ConstraintCreator {
    override def compute(answers: Int): scala.Traversable[Constraint] = Seq(Required, Max(answers + 1))
  }

  val closedAnswerSetCC = new ConstraintCreator {
    override def compute(answers: Int): scala.Traversable[Constraint] = Seq(Max(answers + 1))
  }

  // ops
  private class ThrowIfFailureOps(v: ValidationNel[String, _]) {
    def throwIfFailure(message: String): Unit = {
      v match {
        case Failure(f) => throw new IllegalArgumentException(s"${message}: " + f.toList.mkString(", "))
        case _ =>
      }
    }
  }

  private object ThrowIfFailureOps {
    implicit def createNel(v: ValidationNel[String, _]) = new ThrowIfFailureOps(v)

    implicit def create(v: Validation[String, _]) = new ThrowIfFailureOps(v.toValidationNel)
  }

  // questions


  /**
   *
   * @param id the uniquie identifier of the question: must be unique across all ids in the entire survey
   * @param text the question text
   * @param hint the question hint text
   * @param secondaryText the secondary question text
   * @param constraints the question's constraints. see [[survey.constraints.Constraint]]
   * @return a TextEntry
   */
  def textEntry(id: String, text: Option[String], hint: Option[String], secondaryText: Option[String] = None,
                constraints: Traversable[Constraint] = Required): SimpleQuestion = {
    checkValidId(id).throwIfFailure("id")
    SimpleQuestion("TextEntry", id, text, constraints, hint, secondaryText)
  }

  def textArea(id: String, text: Option[String], hint: Option[String], secondaryText: Option[String] = None,
                constraints: Traversable[Constraint] = Required): SimpleQuestion = {
    checkValidId(id).throwIfFailure("id")

    SimpleQuestion("TextArea", id, text, constraints, hint, secondaryText)
  }

  def buttonOption(id: String, text: Option[String]): SelectOption = {
    checkValidId(id).throwIfFailure("id")

    SelectOption("ButtonOption", id, text, Optional)
  }

  /**
   *
   * @param id the uniquie identifier of the question: must be unique across all ids in the entire survey
   * @param text the question text
   * @param hint the question hint text
   * @param secondaryText the secondary question text
   * @param constraints the question's constraints. see [[survey.constraints.Constraint]]
   * @return a TagEntry
   */
  def tagEntry(id: String, text: Option[String], hint: Option[String], secondaryText: Option[String] = None,
               constraints: Traversable[Constraint] = Required): SimpleQuestion = {
    checkValidId(id).throwIfFailure("id")
    SimpleQuestion("TagEntry", id, text, constraints, hint, secondaryText)
  }

  /**
   *
   * @param id the uniquie identifier of the question: must be unique across all ids in the entire survey
   * @param text the question text
   * @param hint the question hint text
   * @param secondaryText the secondary question text
   * @param constraints the question's constraints. see [[survey.constraints.Constraint]]
   * @param answers the set of possible answers
   * @return a DropDown
   */
  def dropDown(id: String, text: Option[String], hint: Option[String], secondaryText: Option[String] = None,
               constraints: Traversable[Constraint] = Required)
              (answers: Set[String]): ClosedAnswerSetQuestion = {
    checkValidId(id).throwIfFailure("id")

    val c = mergeConstraints(constraints ++ Max(1))

    requireMax1(c)

    checkConflicts(c).throwIfFailure("constraints")
    ClosedAnswerSetQuestion("DropDown", id, text, answers, c, hint, secondaryText)
  }

  def singleComboSearch(id: String, text: Option[String], hint: Option[String],
                              secondaryText: Option[String] = None, constraints: Traversable[Constraint] = Required)
                             (answers: Set[String]): ClosedAnswerSetQuestion = {
    checkValidId(id).throwIfFailure("id")

    val c = mergeConstraints(constraints ++ Max(1))
    requireMax1(c)

    ClosedAnswerSetQuestion("ComboSearchEntry", id, text, answers, c, hint,
                            secondaryText)
  }

  def multiComboSearch(id: String, text: Option[String], hint: Option[String],
                             secondaryText: Option[String] = None, constraints: Traversable[Constraint] = Required)
                            (answers: Set[String]): ClosedAnswerSetQuestion = {
    checkValidId(id).throwIfFailure("id")

    val c = closedAnswerSetCC(answers.size, constraints)
    checkConflicts(c).throwIfFailure("constraints")

    ClosedAnswerSetQuestion("ComboSearchEntry", id, text, answers, closedAnswerSetCC(answers.size, constraints),
                            hint, secondaryText)
  }

  /**
   *
   * @param id the uniquie identifier of the question: must be unique across all ids in the entire survey
   * @param text the question text
   * @param constraints the question's constraints. see [[survey.constraints.Constraint]]
   * @param answers the [[Seq]] of answers. must contain an odd number of answers
   * @return
   */
  def likert(id: String, text: Option[String], constraints: Traversable[Constraint] = Required)
            (answers: Seq[String]): LikertQuestion = {
    checkValidId(id).throwIfFailure("id")

    val c = likertQuestionCC(answers.size, constraints)
    checkConflicts(c).throwIfFailure("constraints")

    require(answers.size % 2 != 0, "answer Seq must contain an odd number of answers")

    LikertQuestion("LikertGroup", id, text, answers, likertQuestionCC(answers.size, constraints))
  }

  def radioGroup(id: String, text: Option[String], secondaryText: Option[String] = None,
                 constraints: Traversable[Constraint] = Required)(answers: Set[String]): ClosedAnswerSetQuestion = {
    checkValidId(id).throwIfFailure("id")

    val c = mergeConstraints(constraints ++ Max(1))
    checkConflicts(c).throwIfFailure("constraints")

    requireMax1(c)

    ClosedAnswerSetQuestion("RadioGroup", id, text, answers, c, None, secondaryText)
  }

  def checkboxGroup(id: String, text: Option[String],

                    secondaryText: Option[String] = None, constraints: Traversable[Constraint] = Required)
                   (answers: Set[String]): ClosedAnswerSetQuestion = {
    checkValidId(id).throwIfFailure("id")

    val c = closedAnswerSetCC(answers.size, constraints)
    checkConflicts(c).throwIfFailure("constraints")

    ClosedAnswerSetQuestion("CheckboxGroup", id, text, answers, closedAnswerSetCC(answers.size, constraints), None,
                            secondaryText)
  }

  def binarySelect(id: String, text: Option[String], answers: Set[String],
                   constraints: Traversable[Constraint] = Required): ClosedAnswerSetQuestion = {
    checkValidId(id).throwIfFailure("id")

    val c = mergeConstraints(constraints ++ Max(1))
    checkConflicts(c).throwIfFailure("constraints")

    require(answers.size == 2, "need two answers")

    ClosedAnswerSetQuestion("BinarySelect", id, text, answers, c, None, None)
  }


  // sections
  def page(id: String, name: Option[String] = None)(elements: Seq[PageElement]): SimplePage = {
    checkValidId(id).throwIfFailure("id")

    checkDuplicateIds(gatherIds(elements)).throwIfFailure(s"page: ${id} has duplicate ids")

    SimplePage("Page", id, elements, name)
  }

  def section(id: String, name: Option[String] = None)(subsections: Seq[Section]): SimpleSection = {
    checkValidId(id).throwIfFailure("id")

    checkDuplicateIds(gatherIds(subsections)).throwIfFailure(s"section: ${id} has duplicate ids")

    SimpleSection("Section", id, subsections, name)
  }

  def subSection(id: String, name: Option[String] = None)(pages: Seq[Page]): SimpleSection = {
    checkValidId(id).throwIfFailure("id")

    checkDuplicateIds(gatherIds(pages)).throwIfFailure(s"subSection: ${id} has duplicate ids")

    SimpleSection("SubSection", id, pages, name)
  }

  def group(id: String, orientation: Orientation = Orientation.Horizontal,
            header: Option[String] = None)(items: Seq[PageElement]): QuestionGroup = {
    checkValidId(id).throwIfFailure("id")

    checkDuplicateIds(gatherIds(items)).throwIfFailure(s"question group: ${id} has duplicate ids")

    QuestionGroup("QuestionGroup", id, items, header, orientation)
  }

  //can decline the other questions in the group by selecting the condition
  def conditionalDeclineGroup(id: String, condition: String, orientation: Orientation = Orientation.Horizontal,
                       header: Option[String] = None)(questions: Seq[BaseQuestion]): ConditionalQuestionGroup = {
    checkValidId(id).throwIfFailure("id")

    checkDuplicateIds(gatherIds(questions)).throwIfFailure(s"conditional group: ${id} has duplicate ids")

    ConditionalQuestionGroup("ConditionalDeclineQuestionGroup", id, condition, questions, header, orientation)
  }

  def conditionalAnswerGroup(id: String, condition: String, orientation: Orientation = Orientation.Horizontal,
                              header: Option[String] = None)(questions: Seq[BaseQuestion]): ConditionalQuestionGroup = {
    checkValidId(id).throwIfFailure("id")

    checkDuplicateIds(gatherIds(questions)).throwIfFailure(s"conditional group: ${id} has duplicate ids")

    ConditionalQuestionGroup("ConditionalAnswerQuestionGroup", id, condition, questions, header, orientation)
  }

  def items(items: PageElement*): Seq[PageElement] = items

  def questions(questions: BaseQuestion*): Seq[BaseQuestion] = questions

  def pages(pages: Page*): Seq[Page] = pages

  def sections(sections: Section*): Seq[Section] = sections

  def answers(answers: String*): Seq[String] = answers

  case object begin {
    def survey(id: String, name: String, description: String, children: Seq[Section]): Survey = {
      checkValidId(id).throwIfFailure("id")

      checkDuplicateIds(id :: gatherIds(children)).throwIfFailure(s"survey: ${id} has dupliate section ids")

      Survey("Survey", id, name, description, children)
    }
  }
}
