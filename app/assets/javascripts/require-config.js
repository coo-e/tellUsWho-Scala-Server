//noinspection Annotator
require.config({
    //setup paths to the webjar assets
    paths: {
        jquery: "../lib/jquery/jquery.js",
        bootstrap: "../lib/bootstrap/dist/js/bootstrap",
        react: "../lib/react/react-with-addons",
        fluxxor: "../lib/fluxxor/fluxxor",
        ReactBootstrap: "../lib/react-bootstrap/react-bootstrap",
        ReactRouter: "../lib/react-router/ReactRouter",
        superagent: "../lib/superagent/lib/client"
    },

    //bootstrap requires jquery to be loaded first. this causes that to happen
    shim: {
        "bootstrap": { deps: ["jquery"]}
    }
});
