require(["react", "rjs/happy"], function (React, Happy) {
    React.render(
        <Happy message="Test" />,
        document.getElementById("content")
    );
});