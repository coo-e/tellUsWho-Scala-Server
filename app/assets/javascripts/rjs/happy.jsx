define(["react"], function (React) {
    return React.createClass({
        render: function () {
            return (
                <h1>Happy {this.props.message}</h1>
            );
        }
    });
});