package controllers

import java.io.File
import javax.inject.Inject

import javax.inject.Inject

import com.mohiva.play.silhouette.api.{ Environment, LogoutEvent, Silhouette }
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import com.mohiva.play.silhouette.impl.providers.SocialProviderRegistry
import forms._
import models.User
import play.api.i18n.MessagesApi

import scala.concurrent.Future

/**
 * The basic application controller.
 *
 * @param messagesApi The Play messages API.
 * @param env The Silhouette environment.
 * @param socialProviderRegistry The social provider registry.
 */
class ApplicationController @Inject() (
                                        val messagesApi: MessagesApi,
                                        val env: Environment[User, CookieAuthenticator],
                                        socialProviderRegistry: SocialProviderRegistry)
  extends Silhouette[User, CookieAuthenticator]{

  /**
   * Handles the index action.
   *
   * @return The result to display.
   */
  def index = {logger.info("gfdsgfsgs")
    SecuredAction.async {
      implicit request =>
        Future.successful(Ok(views.html.home(request.identity)))
    }}

  def html(file: String) = SecuredAction.async {
      //val current = new java.io.File(".").getCanonicalPath
      //logger.error(current)
    implicit request =>
    val identity = request.identity.email.get
    logger.error(s"herreee is my identity???: $identity")
      val f = new File(s"ui/app/$file")
      if (f.exists())
        Future.successful(Ok(scala.io.Source.fromFile(f.getCanonicalPath()).mkString).as("text/html"))
      else
        Future.successful(Ok("wtf yo"))
  }




  /**
   * Handles the Sign In action.
   *
   * @return The result to display.
   */
  def signIn = UserAwareAction.async { implicit request =>
    request.identity match {
      case Some(user) => Future.successful(Redirect(routes.ApplicationController.index()))
      case None => Future.successful(Ok(views.html.signIn(SignInForm.form, socialProviderRegistry)))
    }
  }

  /**
   * Handles the Sign Up action.
   *
   * @return The result to display.
   */
  def signUp = UserAwareAction.async { implicit request =>
    request.identity match {
      case Some(user) => Future.successful(Redirect(routes.ApplicationController.index()))
      case None => Future.successful(Ok(views.html.signUp(SignUpForm.form)))
    }
  }

  /**
   * Handles the Sign Out action.
   *
   * @return The result to display.
   */
  def signOut = SecuredAction.async { implicit request =>
    val result = Redirect(routes.ApplicationController.index())
    //env.eventBus.publish(LogoutEvent(request.identity, request, request2lang))

    env.authenticatorService.discard(request.authenticator, result)
  }
}
