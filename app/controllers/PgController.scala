package controllers

/**
 * Created by kingHenry on 6/2/15.
 */

import javax.inject.Inject

import akka.actor.Identify
import com.mohiva.play.silhouette.api.{ Environment, LogoutEvent, Silhouette }
import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.impl.authenticators.{CookieAuthenticator, SessionAuthenticator}
import com.mohiva.play.silhouette.impl.providers.SocialProviderRegistry
import models.User
import play.api.i18n.{Messages, MessagesApi, I18nSupport}
import survey._
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc.Action
import controllers.Application._
import play.api._
import play.api.Logger
import play.api.mvc._

import play.api.libs.json._

import scala.concurrent.Future
class PgController @Inject() ( val messagesApi: MessagesApi,
                               val env: Environment[User, CookieAuthenticator],
                               socialProviderRegistry: SocialProviderRegistry)
  extends Silhouette[User, CookieAuthenticator] with I18nSupport{

  val DBAction = new DbTest;

//  def retrieveUsersfromDb() = SecuredAction.async{
//    implicit  request =>
//      val results = DBAction.retrieveUsers
//      logger.error(s"$results")
//      Future.successful(Ok(results.toString))
//  }

  def saveUserToDb(email:String,auth:String) = SecuredAction.async{
    implicit request =>
      DBAction.upsertUserToken(email,auth)
      val currentUser = identity(User)
      logger.error(s"current user : $currentUser")
     Future.successful(Ok(s"saved $email + $auth to pgsql!!"))
    //Future.failed(new Throwable ("wtf"))
      }



  def saveReponseToDb(qId:String,userEmail:String,response:String) = SecuredAction.async {
    implicit request =>
      val notAString = qId.toInt
      DBAction.saveResponse(notAString,userEmail,response)
      Future.successful(Ok("Ok"))
  }





  }

