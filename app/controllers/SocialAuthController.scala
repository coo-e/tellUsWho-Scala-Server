package controllers
//WOW SUCH IMPORTS
import javax.inject.Inject

import akka.actor.{ActorSystem, Props, Actor}
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.impl.authenticators.{SessionAuthenticator, CookieAuthenticator}
import com.mohiva.play.silhouette.impl.providers._
import com.mohiva.play.silhouette.impl.providers.oauth2.GoogleProvider
import models.User
import models.services.UserService
//import social.CustomFacebookProvider
import com.mohiva.play.silhouette.api.util.HTTPLayer
import com.mohiva.play.silhouette.impl.providers.{OAuth2Settings, OAuth2StateProvider}
import play.api.i18n.{ MessagesApi, Messages }
import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc.Action
import play.api.libs.json._
import play.api.mvc._
import survey.DbTest

import scala.concurrent.Future
//MANY STUFF

/**
 * The social auth controller.
 *
 * @param messagesApi The Play messages API.
 * @param env The Silhouette environment.
 * @param userService The user service implementation.
 * @param authInfoRepository The auth info service implementation.
 * @param socialProviderRegistry The social provider registry.
 */


class SocialAuthController @Inject() (val messagesApi: MessagesApi,
val env: Environment[User, CookieAuthenticator],
userService: UserService,
authInfoRepository: AuthInfoRepository,
socialProviderRegistry: SocialProviderRegistry)
extends Silhouette[User, CookieAuthenticator] with Logger {
 val system = ActorSystem("mySystem")


//  social.CustomFac



  var userTokenToSave:String = " "
  val regX=""".*OAuth2State=(.*?;).*""".r


  /**
   * Authenticates a user against a social provider.
   *
   * @param provider The ID of the provider to authenticate against.
   * @return The result to display.
   */
  def authenticate(provider: String) = Action.async { implicit request =>
    (socialProviderRegistry.get(provider) match {
      case Some(p: SocialProvider with CommonSocialProfileBuilder) =>
        logger.error(s"provider: $provider")
          p.authenticate().flatMap {
            case Left(result) ⇒ {
              Future.successful(result)}
            case Right(authInfo) => for {
              profile <- p.retrieveProfile(authInfo)
              user <- userService.save(profile)
              authInfo <- authInfoRepository.save(profile.loginInfo, authInfo)
              authenticator <- env.authenticatorService.create(profile.loginInfo)
              value <- env.authenticatorService.init(authenticator)
              result <- env.authenticatorService.embed(value, Redirect(routes.ApplicationController.html("testPushData.html"))
                .withHeaders("X-Content-Type-Options" -> "text/html")
                .withCookies(
                Cookie("firstname",user.firstName.get),
              Cookie("lastname",user.lastName.get),
              Cookie("email",user.email.get),
              Cookie("avatar",user.avatarURL.get)))
            }
              yield {
                val authThing = authInfo.toString
                var regToken: String = ""
                val reg1 = """.*?Auth2Info.(.*?),Some.*""".r
                authThing match {
                  case reg1(xx) => regToken = xx
                }
                val header = result.header
                val headers = header.headers("Set-Cookie")
                val json: JsValue = JsObject(Seq(
                  "firstName" -> JsString(user.firstName.get),
                  "lastName" -> JsString(user.lastName.get),
                  "email" -> JsString(user.email.get),
                  "sessAuthToken" -> JsString(regToken)
                ))
                logger.error(s"JsonForReact : $json")
                val email = user.email.get
                val fName = user.firstName.get
                val lName = user.lastName.get
                //val url = s"http://localhost:9000/save/$email/$fName/$lName"
                //currently inefficient workaround, as WsURL was not completing in the desired scope
                val theTest = new DbTest
                theTest.upsertUserToken(email,regToken)
                result
              }
          }
      case _ => Future.failed(new ProviderException(s"Cannot authenticate with unexpected social provider $provider"))
    }).recover {
      case e: ProviderException =>
        logger.error("Unexpected provider error", e)
        Redirect(routes.ApplicationController.signIn()).flashing("error" -> Messages("could.not.authenticate"))
    }
  }
  val listener = system.actorOf(Props(new Actor {
    def receive = {
      case e @ LoginEvent(identity: User , request, lang) => println(e)
      case e @ LogoutEvent(identity: User, request, lang) => println(e)
    }
  }
  )


  )

}
