package controllers

/**
 * Created by kingHenry on 5/21/15.
 */


import javax.inject.Inject

import com.sun.corba.se.spi.ior.Writeable
import scala.concurrent.Future

import play.api.libs.ws._
import models._
import com.mohiva.play.silhouette.api.{ Environment, LogoutEvent, Silhouette }
import com.mohiva.play.silhouette.impl.authenticators.{CookieAuthenticator, JWTAuthenticator, JWTAuthenticatorService, SessionAuthenticator}
import com.mohiva.play.silhouette.impl.providers._
import play.api.Logger
import play.api.data.Form
import play.api.i18n.{MessagesApi,Messages,I18nSupport}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import play.api.mvc.Action

import play.api.data.Forms._
import play.api.libs.json._
import scala.concurrent.Future
import scala.util.parsing.json.JSONArray

class ResponseController @Inject() ( val messagesApi: MessagesApi,
                                     val env: Environment[User, CookieAuthenticator],
                                     socialProviderRegistry: SocialProviderRegistry,
                                     ws:WSClient)
  extends Silhouette[User, CookieAuthenticator] with I18nSupport {

  /**
   * The core logic of saving a reponse for an authenticated user
   * request.identity retrieves attributes of the logged in user
   * Here we are requesting e-mail
   *
   * We are binding to the form and use play fold to respond to one of two actions
   *  In this case 1) error, 2) the response POSTs successfully
   *  We are then calling a jsonSave DB method to save the response as JSONB in the 'responses' table
   *
   *  This method is the beginning of learning to save responses
   *  Does not accept a easily malleable structure that would be easy for React UI to replicate
   */
  def newResponse = SecuredAction { implicit request =>
    responseForm.bindFromRequest.fold(
      errors => {
        logger.error(s"$errors")
        BadRequest(views.html.responses(errors))
      },
      response => {
        Response.jsonSave(response, request.identity.email.get )
        Redirect(routes.ResponseController.responses).flashing("success" -> "Response saved successfully!")
      }
    )
  }

  /**
   * Method for accepting a structure of array of JSON objects as responses to question(s)
   * The way this is formatted, one question will be rendered as :
   *
   * [{"useremail":"email",
   *    "questionid":id,
   *     "id":id,
   *     "response":"response"}]
   *
   *     This format itself needs to be polished/edited following discussion with other developers
   *
   *     Using a for-comprehension allows for processing of 1-n number of responses
   *     The implicit JSON reader in the Response companion Object parses the POST'ed JSON data
   *     into a format that is able to be saved into pgSQL
   *
   *     (see models/Response.scala)
   * @param email
   *  Using a secured action was not working for some reason at least when just implementing the postJSONTest
   *  unit test, so as a temporary workaround we are passing the user's email from a ws.url call to our api directly
   * @return
   */

  def postResponse(email:String) = SecuredAction {
    implicit request =>
        val usersEmail=request.identity.email.get
        val json = request.body.asJson.get
        val jsonList = json.as[List[JsObject]]
        for (item <- jsonList) {
            val oneItem = item.as[Response]
            Response.jsonSave(oneItem,usersEmail)
            }
        Ok("success")
  }

  /**
   * Unit test for testing successful POST of JSON data to postResponse() method
   *
   * Reason we cannot pass easily to a secured JSON post action is because ws does not allow us to authenticate easily
   * Should be easy to work around with the browser fully authed, however
   * we are currently using a /:email handle in the conf.routes file to pass the currently authorized user's
   * email address
   * @return
   */
  def postJSONTest = SecuredAction {
    implicit request =>

      val datum1 = Json.obj(
        "useremail" -> "harirao3@gmail.com",
        "questionid" -> 1,
        "id" -> 1,
        "response" -> "hello"
      )

      val datum2 = Json.obj(
        "useremail" -> "harirao3@gmail.com",
        "questionid" -> 2,
        "id" -> 2,
        "response" -> "anotherAnswer?"
      )

      val data = JsArray(Seq(datum1,datum2))
      val email = request.identity.email.get
      val url = s"http://connections.srihari.guru:9000/response/save/$email"
      val futureResponse: Future[WSResponse] = ws.url(url).post(data)
      logger.error(s"I think it did stuffs..$url,$data")
      Redirect(routes.ResponseController.responses).flashing("success" -> "JSON posted!")
  }


  /**
   * static view pages containing forms for self use of testing various save methods
   *
   * @return
   */
  def responses = Action {
    Ok(views.html.responses(responseForm))
  }

  def demographics = Action {
    Ok(views.html.demographics(demographicsForm))
  }

  def passions = Action {
    Ok(views.html.passions(passionsForm))
  }


  /**
   * various methods of retrieving/saving background info, such as demographics, passions etc.
   * Will need to restructure this logic as the data model changes to fit Android client rendering desires
   * as well as data structure optimization for hypothetical match data on the android clients
   * @return
   */


  def readPassions = SecuredAction {
    implicit request =>
    passionsForm.bindFromRequest.fold(
    errors => {
      logger.error(s"$errors")
      BadRequest(views.html.passions(errors))
    },
    response => {
      val usersEmail = request.identity.email.get
      val lastName = request.identity.lastName.get
      val firstName = request.identity.firstName.get
      logger.error(s"${response}")
      print(s"names: $usersEmail,$firstName,$lastName")
      Passions.appendPassions(response,usersEmail,firstName,lastName)
      Redirect(routes.ResponseController.passions).flashing("success" -> "Response saved successfully!")
    }
    )
  }
//
//  def readInterests = SecuredAction {
//    implicit request =>
//      interestsForm.bindFromRequest.fold(
//        errors => {
//          logger.error(s"$errors")
//          BadRequest(views.html.interests(errors))
//        },
//        response => {
//          val arrayOfintAttribs = response.interestAttributes.split(",")
//          print(arrayOfintAttribs {
//            0
//          })
//          arrayOfintAttribs.map(Question.create(_))
//          Redirect(routes.ResponseController.interests).flashing("success" -> "Response saved successfully!")
//        })
//  }


  def saveBgInfo = SecuredAction {
    implicit request =>
      demographicsForm.bindFromRequest.fold(
        errors => {
          logger.error(s"$errors")
          BadRequest(views.html.demographics(errors))
        },
        response => {
          val usersEmail = "harirao3@gmail.com"
          val userEmail = request.identity.email.get
          val firstN = request.identity.firstName.get
          val lastN = request.identity.lastName.get
          val stringResponse = response.toString()
          Demographics.saveBackground(response,userEmail,firstN,lastN)
          Redirect(routes.ResponseController.responses).flashing("success" -> "Response saved successfully!")
        })
  }

  /**
   * Just a bunch of forms to use in scala.html files for personal testing purposes, should eventually be removed
   *
   */

val passionsForm : Form[Passions] = Form(mapping(
  "passion" -> nonEmptyText,
  "passionLevel" -> longNumber,
  "expertiseLevel" -> longNumber,
  "questionId" -> longNumber,
  "willingToTeach" -> boolean,
  "lookingForOthers" -> boolean,
  "onCampus" -> optional(text),
  "offCampus" -> optional(text)
  )(Passions.apply)(Passions.unapply))


  val responseForm: Form[Response] = Form(mapping(
    "useremail" -> nonEmptyText,
    "questionid" -> longNumber,
    "id" -> longNumber,
    "response" -> nonEmptyText)(Response.apply)(Response.unapply))

  val demographicsForm: Form[Demographics] = Form(mapping(
    "questionId" -> longNumber,
    "email" -> nonEmptyText,
    "firstName" -> nonEmptyText,
    "lastName" -> nonEmptyText,
    "age" -> longNumber,
    //"gender" -> nonEmptyText,
    "nationality" -> nonEmptyText,
    "hometown" -> nonEmptyText,
    "native_language" -> nonEmptyText,
    "other_langs" -> nonEmptyText,
    "relationship_status" -> nonEmptyText,
    "sexual_orientation" -> nonEmptyText,
    "student_type" -> nonEmptyText,
    "commuter" -> boolean,
    "current_city" -> nonEmptyText)(Demographics.apply)(Demographics.unapply))


}

