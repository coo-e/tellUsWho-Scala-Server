package controllers


import javax.inject.Inject



import models.{Passions, User, Question}

import com.mohiva.play.silhouette.api.{ Environment, LogoutEvent, Silhouette }
import com.mohiva.play.silhouette.impl.authenticators.{CookieAuthenticator, JWTAuthenticator, JWTAuthenticatorService, SessionAuthenticator}
import com.mohiva.play.silhouette.impl.providers._
import play.api.Logger
import play.api.data.Form
import play.api.i18n.{MessagesApi,Messages,I18nSupport}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc.Action

import play.api.data.Forms._

import play.api.libs.json._
import views.html.defaultpages.todo


import scala.concurrent.Future

/**
 * Created by kingHenry on 8/25/15.
 */
class SurveyController @Inject() ( val messagesApi: MessagesApi,
                                   val env: Environment[User, CookieAuthenticator],
                                   socialProviderRegistry: SocialProviderRegistry)
  extends Silhouette[User, CookieAuthenticator] with I18nSupport {



  def retrieveQuestions(surveyId:String) = Action {
    val questions = surveys.active.get(surveyId)
    questions match{
      case Some(questions) => {
        val jsonString = questions.asJson
        Ok(jsonString)
      }
      case None => {logger.error(s"mad error")
        Ok("page not found bro")}
    }
  }
}
