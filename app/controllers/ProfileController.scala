package controllers
import javax.inject.Inject



import models.{Demographics, Passions, Users, Question,User}

import com.mohiva.play.silhouette.api.{ Environment, LogoutEvent, Silhouette }
import com.mohiva.play.silhouette.impl.authenticators.{CookieAuthenticator, JWTAuthenticator, JWTAuthenticatorService, SessionAuthenticator}
import com.mohiva.play.silhouette.impl.providers._
import play.api.Logger
import play.api.data.Form
import play.api.i18n.{MessagesApi,Messages,I18nSupport}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc.Action

import play.api.data.Forms._

import play.api.libs.json._


import scala.concurrent.Future
/**
 * Created by kingHenry on 8/24/15.
 */
class ProfileController @Inject() ( val messagesApi: MessagesApi,
                                       val env: Environment[User, CookieAuthenticator],
                                       socialProviderRegistry: SocialProviderRegistry)
    extends Silhouette[User, CookieAuthenticator] with I18nSupport {

  //    implicit val musics = new Writes[Question] {
  //      def writes(question:Question) = Json.obj(
  //        "questionID"-> question.seqQuestionID,
  //        "questionText" -> question.questionText,
  //        "responseURI" -> question.responseURI,
  //        "constraints: min,max,Optional" -> Json.arr(question.minResponse, question.maxResponse, question.optionalQ),
  //        "responseDataType" -> question.responseDataType
  //      )
  //    }



  def deleteQuestion(id: Long) = Action {
    Question.delete(id)
    Redirect(routes.QuestionController.questions)
  }

  def retrieveUsers = Action {
    implicit request =>
      val users = Users.all()
      Ok(Json.toJson(users))
  }
  def retrieveAuthToken = SecuredAction {
    implicit request=>
      val userEmail = request.identity.email.get
      val token = Users.getToken(userEmail)
      Ok(Json.toJson(token))
  }
  def retrieveBackground = SecuredAction {
    implicit request =>
      val userEmail = request.identity.email.get
      val userBackground = Demographics.getBackground(userEmail)
      Ok(Json.toJson(userBackground))
  }
  /**
   * select id,question,questionsection from questions where questionsection='Background'
   */
  def retrievePassions = SecuredAction {
    implicit request =>
      val userEmail = request.identity.email.get
      val userPassions = Passions.getPassions(userEmail)
      Ok(Json.toJson(userPassions))
  }


}
