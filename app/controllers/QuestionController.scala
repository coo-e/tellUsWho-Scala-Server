package controllers

/**
 * Created by kingHenry on 5/21/15.
 */


import javax.inject.Inject


import jdk.nashorn.api.scripting.JSObject
import survey.sections.Survey
import models.{User, Question}
import survey._
import survey.SurveyElement
import com.mohiva.play.silhouette.api.{ Environment, LogoutEvent, Silhouette }
import com.mohiva.play.silhouette.impl.authenticators.{CookieAuthenticator, JWTAuthenticator, JWTAuthenticatorService, SessionAuthenticator}
import com.mohiva.play.silhouette.impl.providers._
import play.api.Logger
import play.api.data.Form
import play.api.i18n.{MessagesApi,Messages,I18nSupport}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc.Action

import play.api.data.Forms._
import models.Question

import play.api.libs.json._
import surveys.backgroundTest


import scala.concurrent.Future

class QuestionController @Inject() ( val messagesApi: MessagesApi,
                                     val env: Environment[User, CookieAuthenticator],
                                     socialProviderRegistry: SocialProviderRegistry)
  extends Silhouette[User, CookieAuthenticator] with I18nSupport{

//    implicit val musics = new Writes[Question] {
//      def writes(question:Question) = Json.obj(
//        "questionID"-> question.seqQuestionID,
//        "questionText" -> question.questionText,
//        "responseURI" -> question.responseURI,
//        "constraints: min,max,Optional" -> Json.arr(question.minResponse, question.maxResponse, question.optionalQ),
//        "responseDataType" -> question.responseDataType
//      )
//    }


  def questions = Action {
    Ok(views.html.question(Question.all(), questionForm))
  }


  def newQuestion = Action { implicit request =>
    logger.error(s"wtf")
    questionForm.bindFromRequest.fold(
      errors => {logger.error(s"$errors")
        BadRequest(views.html.question(Question.all(), errors))
      },
     question => {
       logger.error(s"somethign happens here")
      Question.create(question)
      Redirect(routes.QuestionController.questions).flashing("success" -> "Question saved successfully!")
    }
    )
  }

  def deleteQuestion(id: Long) = Action {
    Question.delete(id)
    Redirect(routes.QuestionController.questions)
  }

  /**
   * select id,question,questionsection from questions where questionsection='Background'
   */
  def retrieveQuestions(section:String) = Action {
    val sectionQuestions = Question.questionSection(section)
    Ok(Json.toJson(sectionQuestions))
  }

  val questionForm:Form[Question]=Form(mapping(
  "id" -> longNumber,
  "versionid" -> longNumber,
    "questiontext" -> nonEmptyText,
  "questionSection"->nonEmptyText,
  "questionType"->nonEmptyText)(Question.apply)(Question.unapply)
  )

}
