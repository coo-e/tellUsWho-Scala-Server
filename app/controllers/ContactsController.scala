package controllers

/**
 * Created by kingHenry on 8/31/15.
 */

import javax.inject.Inject

import com.sun.corba.se.spi.ior.Writeable
import scala.concurrent.Future

import play.api.libs.ws._
import models._
import com.mohiva.play.silhouette.api.{ Environment, LogoutEvent, Silhouette }
import com.mohiva.play.silhouette.impl.authenticators.{CookieAuthenticator, JWTAuthenticator, JWTAuthenticatorService, SessionAuthenticator}
import com.mohiva.play.silhouette.impl.providers._
import play.api.Logger
import play.api.data.Form
import play.api.i18n.{MessagesApi,Messages,I18nSupport}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import play.api.mvc.Action

import play.api.data.Forms._
import play.api.libs.json._
import scala.concurrent.Future
import scala.util.parsing.json.JSONArray

class ContactsController @Inject() ( val messagesApi: MessagesApi,
                                     val env: Environment[User, CookieAuthenticator],
                                     socialProviderRegistry: SocialProviderRegistry,
                                     ws:WSClient)
  extends Silhouette[User, CookieAuthenticator] with I18nSupport {

  def getContacts(email:String) = Action {
    implicit request =>
      val allContacts = Contacts.allContacts(email)
      Ok(Json.toJson(allContacts))
  }
  def postContacts() = Action {
    implicit request =>
      val json = request.body.asJson.get
      val jsonList = json.as[List[JsObject]]
      for (item <- jsonList) {
        val oneItem = item.as[Contacts]
        Contacts.jsonSave(oneItem)
      }
      Ok("success")
  }

//  def upload = Action(parse.multipartFormData) {
//    request =>
//      request.body.file("picture").map { picture =>
//        import java.io.File
//        val filename = picture.filename
//        val contentType = picture.contentType
//        picture.ref.moveTo(new File(s"/tmp/picture/$filename"))
//        Ok("File uploaded")
//      }.getOrElse {
//        //Redirect(routes.Application.index).flashing(
//         Ok( "errorMissing file")
//        }
//  }


}


