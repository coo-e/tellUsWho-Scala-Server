package surveys

import survey.constraints.{Optional, Min, Max, Required}
import survey.dsl._

object backgroundTest {
  val dummyAnswers = answers("one", "two", "three")
  val genders = dummyAnswers.toSet
  val nationality = dummyAnswers.toSet
  val nativeLanguage = dummyAnswers.toSet

  def apply = survey

  val survey = begin survey("backgroundTest_1", "Background Test", "testing background survey",
    sections(
              subSection("basicInfo", name = "Basic Info") {
                pages(
                       page("background", name = "Background") {
                         items(
                                group("backgroundFirstLine") {
                                  questions(
                                             textEntry("birthDate", text = "What's your date of birth",
                                                       hint = "mm/dd/yyyy"),
                                             dropDown("gender", text = "What geneder do you identify with?",
                                                      hint = "Select a gender") {
                                               genders
                                             }
                                           )
                                },
                                group("backgroundSecondLine") {
                                  questions(
                                             dropDown("nationality", text = "What is your nationality?",
                                                      hint = "Select a nationality") {
                                               nationality
                                             },
                                             dropDown("language", text = "What is your native language?",
                                                      hint = "Select a nationality") {
                                               nativeLanguage
                                             }
                                           )
                                },
                                multiComboSearch("otherLanguages", text = "What other languages do you speak?",
                                                       hint = "Please select language(s)", constraints = Optional) {
                                  nativeLanguage
                                },
                                group("backgroundGrowUp", header = "Where did you group up?") {
                                  questions(
                                             textEntry("city_town", text = None, hint = "City/Town"),
                                             dropDown("state", text = None, hint = "State") {
                                               dummyAnswers.toSet
                                             },
                                             dropDown("country", text = None, hint = "Country") {
                                               dummyAnswers.toSet
                                             }
                                           )
                                },
                                group("backgroundLastLine") {
                                  items(
                                    conditionalDeclineGroup("relationshipStatusGroup", "Rather not say")(
                                             questions(dropDown("relationshipStatus", text = "What is your relationship status?",
                                                      hint = "Select a relationship status", constraints = Optional) {
                                               dummyAnswers.toSet
                                             })),
                                    conditionalDeclineGroup("sexualIdentificationGroup", "Rather not say")(
                                      questions(dropDown("sexualIdentification",
                                                          text = "What is your extual identification?", hint = "Select one",
                                                          constraints = Optional) { dummyAnswers.toSet }
                                               ))
                                       )
                                }
                              )
                       },
                       page("page2", name = "Page Two") {
                           items(

                                textEntry("text_test", text = "TextEntry test", hint = "Enter Text", secondaryText = "Other text"),
                                textArea("text_area", text = "TextArea test", hint = "Area"),
                                tagEntry("tag_entry_test", text = "TagEntry test", hint = "Tags"),
                                dropDown("dropdown_text", text = "DropDown test", hint = "Drops")(dummyAnswers.toSet)
                              )
                       },
                      page("page3", name = "Page Three") {
                        items(
                               singleComboSearch("singlecombo_test", text = "Single ComboSearchEntry", hint = "pick")(dummyAnswers.toSet),
                               multiComboSearch("multicombo_test", text = "Multi ComboSearchEntry", hint = "pick more")(dummyAnswers.toSet),
                               likert("likert", text = "How much this sucks")(answers("A lot", "Somewhat", "Ok", "Good", "Best Ever")),
                               radioGroup("radiogroup", text = "RadioGroup test")(dummyAnswers.toSet),
                               checkboxGroup("checkboxgroup", text = "CheckboxGroup")(dummyAnswers.toSet),
                               binarySelect("binaryselect", text = "Which one?", answers("Good", "Evil").toSet)
                             )
                      }
                 )
              }
            )
    )
}
