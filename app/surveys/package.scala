import survey.sections.Survey

package object surveys {

  import scala.language.implicitConversions

  private implicit def surveyKeyValue(survey: Survey) = (survey.surveyId -> survey)

  // just a map of the currently active surveys
  val active: Map[String, Survey] = Map(backgroundTest.survey)

  // SurveyInfo of the surveys that can be seralized to json
  val infos = active.values.map(SurveyInfo(_))

}

package surveys {

import play.api.libs.json.{JsValue, Writes}

case class SurveyInfo(surveyId: String, name: String, description: String)

  object SurveyInfo {
    def apply(survey: Survey): SurveyInfo = SurveyInfo(survey.surveyId, survey.name, survey.description)

    implicit def writes = new Writes[SurveyInfo] {
      import survey.tojson._
      override def writes(o: SurveyInfo): JsValue = o.toJson
    }
  }
}