package models

/**
 * Created by kingHenry on 7/16/15.
 */

import anorm._
import anorm.SqlParser._
import play.api.db._
import play.api.Play.current
import scala.language.postfixOps
import org.postgresql.util._
import play.api.libs.json._

/**
 * Created by kingHenry on 7/16/15.
 */
/**
 * The response to a survey question
 *
 * @param useremail The email of user who submitted this response
 * @param questionid the id # of the question to which the response was submitted
 * @param id the id # of this response
 * @param response the actual json.stringified response submitted by the user
 */


case class Response (useremail:String,
                     questionid:Long,
                     id:Long,
                     response:String)


object Response {
  /**
   * the implicit reader responseReads converts incoming JSON with
   * keys equal to the property names of the Response case class
   * into a format that is easily saved to pgSQL
   */
  implicit val responseReads=Json.reads[Response]


  val response = {
    get[String]("useremail") ~
      get[Long]("questionid") ~
    get[Long]("id") ~
      get[String]("response") map {
      case  useremail~ questionid ~ id ~ response => Response(useremail, questionid, id, response)
    }

  }


  def all(): List[Response] = DB.withConnection { implicit c =>
    SQL("select * from responses").as(response *)
  }

  def delete(id:Long) {
    DB.withConnection{
      implicit c =>
        SQL("delete from responses where id = {id}").on(
          'id -> id
        ).executeUpdate()
    }
  }

  def save(userResponse:String,userEmail:String,qId:Int) = {
    DB.withConnection{
      implicit c =>
        SQL(s"INSERT INTO responses(response,useremail,questionid) values ({userResp},{eMail},{quesId})")
          .on('userResp -> s"""{"response":"$userResponse"}""", 'eMail -> s"$userEmail",'quesId -> qId)
          .executeInsert(SqlParser.scalar[String].singleOpt)
    }
  }

  def jsonSave(usersResponse:Response,email:String) = {
    DB.withConnection{
      implicit c =>
        val userResponse=usersResponse.response
        val userId = usersResponse.id
        val userJson:String = s"""{"response":"$userResponse"}"""
        val pgObject = new PGobject();
        pgObject.setType("jsonb")
        pgObject.setValue(userJson)
        SQL(s"INSERT INTO responses(response,useremail,questionid) values ({userResp},{eMail},{quesId})")
          .on('userResp -> anorm.Object(pgObject), 'eMail -> s"$email",'quesId -> userId)
          .executeInsert(SqlParser.scalar[String].singleOpt)
    }
  }


//  def jsonSave(usersResponse:Response,passions:PassionLevels,email:String,id:Int) = {
//    DB.withConnection{
//      implicit c =>
//        val userResponse=usersResponse.response
//        val interest = passions.interest.interestType
//        val interestAttribs = passions.interest.interestAttributes
//        val passion = passions.activity.passionLevel
//        val expertise = passions.activity.expertiseLevel
//        val teach = passions.activity.willingToTeach
//        val offCampus = passions.activity.offCampus
//        val onCampus = passions.activity.onCampus
//        val others = passions.activity.lookingForOthers
//        //JSON response
//        val userJson:String =
//          s"""
//             |{"${interest}":
//             |      {"passion":"${passion}",
//             |       "expertise":"${expertise},
//             |       "teaching":"${teach}",
//             |       "lookingForOthers":"${others}",
//             |       "location":{"offCampus":"${offCampus}",
//             |                   "onCampus:"${onCampus}"
//             |                  }
//             |      }
//             |
//             |}
//           """.stripMargin
//        val pgObject = new PGobject();
//        pgObject.setType("jsonb")
//        pgObject.setValue(userJson)
//        SQL(s"INSERT INTO responses(response,useremail,questionid) values ({userResp},{eMail},{quesId})")
//          .on('userResp -> anorm.Object(pgObject), 'eMail -> s"$email",'quesId -> id)
//          .executeInsert(SqlParser.scalar[String].singleOpt)
//
//    }
//  }

}



