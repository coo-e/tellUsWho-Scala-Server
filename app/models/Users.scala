package models

import anorm.SqlParser._
import anorm._
import org.postgresql.util.PGobject
import play.api.db.DB
import play.api.Play.current
import play.api.libs.json._
import scala.language.postfixOps
import scala.collection.mutable._

/**
 * Created by kingHenry on 8/3/15.
 */

case class Contacts(name:String,
                     pictureUrl:String,
                     email:String)


object Contacts {
  implicit val contactsReader = Json.reads[Contacts]
  implicit val contactsWriter=Json.writes[Contacts]

  val contacts = {
    get[String]("name") ~
      get[String]("pictureUrl") ~
      get[String]("email") map {
      case name ~ pictureUrl ~ email
      => Contacts(name,pictureUrl,email)
    }
  }
  def allContacts (email:String) : List[Contacts] = DB.withConnection{
    implicit c =>
      SQL(s"select * from contacts where contactemail='$email'").as(contacts *)
  }
  def jsonSave(usersContacts:Contacts) = {
    DB.withConnection{
      implicit c =>
        val usersEmail = "harirao3@gmail.com"
        val contactName=usersContacts.name
        val contactPic = usersContacts.pictureUrl
        val contactEmail = " "
        SQL(s"INSERT INTO contacts(useremail,name,pictureurl,contactemail) values ({uEmail},{uName},{url},{eMail})")
          .on('uEmail-> usersEmail, 'uName -> contactName,'url -> contactPic, 'eMail -> contactEmail)
          .executeInsert(SqlParser.scalar[String].singleOpt)
    }
  }
}

case class Users (email:String,
                   sessauth:String)

object Users {

  implicit val userReads=Json.reads[Users]
  implicit val userWrites=Json.writes[Users]

  val users = {
      get[String]("email") ~
      get[String]("sessauth")  map {
        case email ~ sessauth
      => Users(email,sessauth)
    }
  }
  def getToken(email:String):String = {
    DB.withConnection  {
      implicit c =>
        val result:String = SQL(s"select sessauth from users WHERE email = '$email'").as(scalar[String].single)
        return result
    }
  }


  def all(): List[Users] = DB.withConnection { implicit c =>
    SQL("select email,sessauth from users").as(users *)
  }
}



case class Passions(passion:String,
                    passionLevel:Long,
                    expertiseLevel:Long,
                    questionId: Long,
                    teaching:Boolean,
                    looking:Boolean,
                    onCampus:Option[String],
                    offCampus:Option[String])

object Passions {
  implicit val passionReads=Json.reads[Passions]
  implicit val passionWrites=Json.writes[Passions]

  val passions = {
    get[String]("passion") ~
      get[Long]("passionLevel") ~
      get[Long]("expertise") ~
      get[Long]("questionId") ~
      get[Boolean]("teaching") ~
      get[Boolean]("looking") ~
      get[Option[String]]("onCampus") ~
      get[Option[String]]("offCampus") map {
      case  passion ~ passionLevel ~ expertise~  questionId ~ teaching ~ looking ~ onCampus ~ offCampus =>
        Passions(passion,passionLevel,expertise, questionId,looking,teaching,onCampus,offCampus)
    }
  }

  def getPassions(email:String):List[Passions] = DB.withConnection {
    implicit c =>
      SQL(s"select * from passions where email='$email'").as(passions *)
  }

  def appendPassions(passions:Passions,email:String,firstName:String,lastName:String) = {
    DB.withConnection{
      implicit c =>
        val passion:String = passions.passion
        val offCampus:String = passions.offCampus.getOrElse("")
        val onCampus:String = passions.onCampus.getOrElse("")
        val expertise:Long = passions.expertiseLevel
        val qId:Long = passions.questionId
        val teach:Boolean = passions.teaching
        val looking:Boolean = passions.looking
        val level:Long = passions.passionLevel
        val id = SQL(s"INSERT INTO passions(passion,passionlevel,expertise,questionid,teaching,looking,oncampus,offcampus,email) select " +
          s"{passion},{passionLevel},{expertise},{questionId},{teaching},{looking},{oncampus},{offcampus},{email}")
          .on('passion -> passion,
            'passionLevel-> level,
            'questionId -> qId,
            'expertise -> expertise,
            'teaching -> teach,
            'looking -> looking,
            'oncampus -> onCampus,
            'offcampus -> offCampus,
            'email -> email).executeUpdate()

    }
  }


}

case class Demographics(questionId:Long,
                         email:String,
                         firstName:String,
                         lastName:String,
                         age:Long,
                         nationality:String,
                         hometown:String,
                         nativeLang:String,
                         otherLangs:String,
                         relStatus:String,
                         sexOrientation:String,
                         studentType:String,
                         commuter:Boolean,
                         currentCity:String)

object Demographics {
  implicit val demoReads=Json.reads[Demographics]
  implicit val demoWrites=Json.writes[Demographics]

  val demographics = {
    get[Long]("questionId") ~
      get[String]("email") ~
      get[String]("firstname") ~
      get[String]("lastname") ~
      get[Long]("age") ~
      get[String]("nationality") ~
      get[String]("hometown") ~
      get[String]("native_lang") ~
      get[String]("other_langs") ~
      get[String]("relationship_status") ~
      get[String]("sex_orientation") ~
      get[String]("student_type") ~
      get[Boolean]("commuter") ~
      get[String]("current_city") map {
      case  questionid ~ email ~ firstName ~ lastName ~ age ~
          nationality ~ hometown ~ nativeLang ~ otherLangs ~
          relStatus ~ sexOrientation ~ studentType ~ commuter ~ currentCity =>
        Demographics(questionid,email,firstName,lastName,age,nationality,hometown,
        nativeLang,otherLangs,relStatus,sexOrientation,studentType,commuter,currentCity)
    }
  }

  def getBackground(email:String):List[Demographics] = DB.withConnection {
    implicit c =>
      SQL(s"select * from demographics where email='$email'").as(demographics *)
  }


  def saveBackground(info:Demographics,email:String,firstName:String,lastName:String) = {
    DB.withConnection{
      implicit c =>
        val questionid = info.questionId
        val id = SQL(s"INSERT INTO demographics(questionid,email,firstname,lastname," +
          s"age,nationality,hometown,native_lang,other_langs,relationship_status,sex_orientation," +
          s"student_type,commuter,current_city) select " +
          s"{qId},{email},{firstN},{lastN},{age},{nationality},{hometown},{native_lang}" +
          s",{other_langs},{relationship_status},{sex_orientation},{student_type}," +
          s"{commuter},{current_city}")
          .on('qId -> info.questionId,
            'email -> info.email,
            'firstN -> info.firstName,
            'lastN -> info.lastName,
            'age -> info.age,
            'nationality -> info.nationality,
            'hometown -> info.hometown,
            'native_lang -> info.nativeLang,
            'other_langs -> info.otherLangs,
          'relationship_status -> info.relStatus,
          'sex_orientation -> info.sexOrientation,
          'student_type -> info.studentType,
          'commuter -> info.commuter,
          'current_city -> info.currentCity).executeInsert()

    }
  }

}







/**
 * todo
 * check if this is used anymore
 */
//
//case class JustPassion(passion:String)
//
//object JustPassion {
//  val justPassion = {
//    get[String]("passions") map {
//      case passions => JustPassion(passions)
//    }
//  }
//
//  def passion():String = DB.withConnection{
//    implicit c =>
//    SQL("select passions::varchar from users where users.email='harirao3@gmail.com'").as(justPassion *).toString()
//  }
//
//  def getPassions():String = {
//    val someString = passion().split("JustPassion\\(")
//    val justArray=someString(1).split("]")
//
//    val theActualString= justArray(0)
//
//    return theActualString
//  }
//
//}
