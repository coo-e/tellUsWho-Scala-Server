package models

import anorm._
import anorm.SqlParser._
import org.postgresql.util.PGobject
import play.api.db._
import play.api.Play.current
import play.api.libs.json.Json
import scala.language.postfixOps

/**
 *
 * @param id The identifier
 * @param versionid the version number of the question;revisions/modifications will increment it (++)
 * @param question the text(body) of the question asked to user
 * @param questionType the type of the question indicating the type of response to be received
 */

case class Question (id:Long,
                     versionid:Long,
                     question:String,
                    questionSection:String,
                     questionType:String)

object Question {
  implicit val questionReads=Json.reads[Question]
  implicit val questionWrites=Json.writes[Question]

  val question = {
    get[Long]("id") ~
      get[Long]("versionid") ~
      get[String]("question") ~
      get[String]("questionSection") ~
      get[String]("questionType") map {
      case id  ~ versionid ~ question ~ questionSection ~ questionType => Question(id, versionid, question, questionSection, questionType)
    }

  }

  def questionSection(section:String):List[Question] = DB.withConnection {
    implicit c =>
    SQL(s"select * from questions where questionsection='$section'").as(question *)
  }

  def all(): List[Question] = DB.withConnection { implicit c =>
    SQL("select * from questions").as(question *)
  }

  def create(question: Question) {
    DB.withConnection {
      implicit c =>
        SQL("insert into questions(questiontype,versionid,question,questionsection) values ({quesType},{versId},{text},{questionSec})").on(
          'text -> question.question,
          'quesType -> question.questionType,
          'versId -> 1.toLong,
          'questionSec -> question.questionSection
        ).executeUpdate()
    }
  }

  def create(intAttrib: String) {
    DB.withConnection {
      implicit c =>
        val question: String = s"My favorite $intAttrib are"
        SQL("insert into questions(questiontype,versionid,question,questionsection) values ({quesType},{versId},{text},{questionSec})").on(
          'text -> question,
          'quesType -> "textentry",
          'versId -> 1.toLong,
          'questionSec -> "Interest"
        ).executeUpdate()
    }
  }

  def delete(id: Long) {
    DB.withConnection {
      implicit c =>
        SQL("delete from questions where id = {id}").on(
          'id -> id
        ).executeUpdate()
    }
  }
}

