package models

/**
 * Created by kingHenry on 5/26/15.
 *
 * constraint is something
 */


trait Constraint {
  /**
   * evaluates the number of answers to match the constraint criteria
   * @param numAnswers
   * @return true if evaluation passes
   */
  def evaluate(numAnswers:Int):Boolean
  /**
   * the constraint that this constraint conflicts with, duh
   * @return sequence of constraints
   */
 //def conflictsWith : Seq[Class[Constraint]] = Seq()
}

/**
 * returns true for any num answers, because its an optional question
 * doesnt conflict with anyhting, cuz, its an optional question
 */
trait Optional extends Constraint {
  override def evaluate(numAnswers: Int): Boolean = true
}

object Optional {
  def apply = new Optional {}
}


case class Max(maxAnswers:Int) extends Constraint {
  override def evaluate(numAnswers: Int): Boolean = numAnswers <= maxAnswers
}

case class Min(minAnswers:Int) extends Constraint {
  override def evaluate(numAnswers: Int): Boolean = numAnswers >= minAnswers

  //override def conflictsWith: Seq[Class[Constraint]] = Seq(Class[Optional])
}