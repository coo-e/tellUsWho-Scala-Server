#!/bin/bash
echo "******CREATING DOCKER DATABASE******"
gosu postgres postgres --single <<- EOSQL
CREATE DATABASE docker;
CREATE USER docker;
GRANT ALL PRIVILEGES ON DATABASE docker TO docker;
echo ""
echo "******DOCKER DATABASE CREATED******"
EOSQL
echo "CREATING TABLES"
gosu postgres postgres --single docker <<- EOSQL
CREATE TABLE questions( \
id serial NOT NULL, \
questiontype text, \
versionid integer, \
question text, \
questionsection text, \
        CONSTRAINT questions_id_key UNIQUE (id)) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE questions OWNER TO docker; \
GRANT ALL ON TABLE questions TO docker; \
CREATE TABLE users \
( \
id serial NOT NULL, \
email text, \
sessauth text, \
CONSTRAINT users_email_key UNIQUE (email) \
) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE users OWNER TO docker; \
GRANT ALL ON TABLE users TO docker; \
CREATE TABLE responses \
( \
response jsonb, \
useremail text, \
questionid integer, \
id serial NOT NULL, \
CONSTRAINT question_id_fk FOREIGN KEY (questionid) \
REFERENCES questions (id) MATCH SIMPLE \
ON UPDATE NO ACTION ON DELETE NO ACTION, \
CONSTRAINT responses_useremail_fkey FOREIGN KEY (useremail) \
REFERENCES users (email) MATCH SIMPLE \
ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
        ); \
ALTER TABLE responses OWNER TO docker; \
GRANT ALL ON TABLE responses TO docker; \
CREATE INDEX fki_question_id_fk \
  ON responses \
  USING btree \
  (questionid); \
CREATE TABLE demographics \
( \
questionid integer, \
email text, \
firstname text, \
lastname text, \
age integer, \
nationality text, \
hometown text, \
native_lang text, \
other_langs text, \
relationship_status text, \
sex_orientation text, \
student_type text, \
commuter boolean, \
current_city text, \
CONSTRAINT demo_question_id_fk FOREIGN KEY (questionid) \
REFERENCES questions(id) MATCH SIMPLE \
ON UPDATE NO ACTION ON DELETE NO ACTION, \
CONSTRAINT demo_useremail_fkey FOREIGN KEY (email) \
REFERENCES users (email) MATCH SIMPLE \
N UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
        ); \
ALTER TABLE demographics OWNER TO docker; \
GRANT ALL ON TABLE responses TO docker; \
CREATE TABLE passions \
( \
passion text, \
  passionlevel integer, \
  expertise integer, \
questionid integer, \
  teaching boolean, \
looking boolean, \
oncampus text, \
offcampus text, \
  email text, \
CONSTRAINT passions_question_id_fk FOREIGN KEY (questionid) \
REFERENCES questions (id) MATCH SIMPLE \
ON UPDATE NO ACTION ON DELETE NO ACTION, \
  CONSTRAINT email_fk FOREIGN KEY (email) \
  REFERENCES users (email) MATCH SIMPLE \
  ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
        WITH ( \
                  OIDS=FALSE \
        ); \
ALTER TABLE passions OWNER TO docker; \
GRANT ALL ON TABLE passions TO docker; \
CREATE TABLE survey \
( \
  id integer NOT NULL, \
  version integer, \
  CONSTRAINT survey_pkey PRIMARY KEY (id) \
) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE survey OWNER TO docker; \
GRANT ALL ON TABLE survey TO docker; \
CREATE TABLE survey_instance \
( \
  useremail text, \
  surveydate DATE, \
  surveyid integer, \
  starttime TIMESTAMP WITH TIME ZONE, \
  endtime TIMESTAMP WITH TIME ZONE, \
  completed boolean, \
  inprogress uuid, \
  CONSTRAINT survey_useremail_fkey FOREIGN KEY (useremail) \
      REFERENCES users (email) MATCH SIMPLE \
      ON UPDATE NO ACTION ON DELETE NO ACTION, \
  CONSTRAINT surveyid_fkey FOREIGN KEY (surveyid) \
      REFERENCES survey (id) MATCH SIMPLE \
      ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
        WITH ( \
                OIDS=FALSE \
        ); \
ALTER TABLE survey_instance OWNER to docker; \
GRANT ALL ON TABLE survey_instance to docker; \
CREATE TABLE contacts \
( \
  useremail text NOT NULL, \
  name text, \
  pictureurl text, \
  contactemail text, \
  CONSTRAINT contacts_pkey PRIMARY KEY (contactemail), \
  CONSTRAINT contacts_email_fkey FOREIGN KEY (useremail) \
        REFERENCES users(email) MATCH SIMPLE \
        ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE contacts OWNER TO docker; \
GRANT ALL ON TABLE contacts TO docker; \
CREATE INDEX fki_contactemail_fk \
  ON contacts \
  USING btree \
  (contactemail);
EOSQL
echo "TABLES CREATED"