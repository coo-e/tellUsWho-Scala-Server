import play.Play.autoImport._
import PlayKeys._
import com.tuplejump.sbt.yeoman.Yeoman

name := """play-scala"""

version := "0.1.0.0"

lazy val root = (project in file(".")).
	enablePlugins(PlayScala).
	settings(
      routesImport ++= Seq("scala.language.reflectiveCalls")
	).
  settings(
    (Yeoman.yeomanSettings ++ Yeoman.withTemplates): _*
  )

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  filters,
  ws,
  specs2 % Test,
  "org.webjars" %% "webjars-play" % "2.4.0",
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "com.typesafe.play" %% "anorm" % "2.4.0",
  "com.mohiva" %% "play-silhouette" % "3.0.0-RC2",
  "net.codingwell" %% "scala-guice" % "4.0.0",
  "net.ceedubs" %% "ficus" % "1.1.2",
  "com.adrianhurt" %% "play-bootstrap3" % "0.4.4-P24",
  "org.scalaz" %% "scalaz-core" % "7.1.3",
  "com.chuusai" %% "shapeless" % "2.2.5"
)

resolvers ++= Seq(
  "Atlassian Releases" at "https://maven.atlassian.com/public/",
  "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
)

routesGenerator := InjectedRoutesGenerator


scalacOptions ++= Seq(
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-language:reflectiveCalls",
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
  "-Xfatal-warnings", // Fail the compilation if there are any warnings.
  "-Xlint", // Enable recommended additional warnings.
  "-Ywarn-adapted-args", // Warn if an argument list is modified to match the receiver.
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-inaccessible", // Warn about inaccessible types in method signatures.
  "-Ywarn-nullary-override", // Warn when non-nullary overrides nullary, e.g. def foo() over def foo.
  "-Ywarn-numeric-widen" // Warn when numerics are widened.
)

//Yeoman.yeomanSettings ++ Yeoman.withTemplates

Yeoman.forceGrunt := false

pipelineStages := Seq(rjs)

// clear out play-yeoman hook since it causes a class not found exception
playRunHooks := Seq()

playRunHooks <+= (Yeoman.yeomanDirectory, Yeoman.yeomanGruntfile).map {
  (a, b) => GruntPlayRunHook(a, b, "dev")
}


fork in run := true
