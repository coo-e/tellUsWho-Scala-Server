FROM postgres:9.4.1
MAINTAINER Srihari Rao <yolo@srihari.guru>

ADD initdb.sh /docker-entrypoint-initdb.d/
