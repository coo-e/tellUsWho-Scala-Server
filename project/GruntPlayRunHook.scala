import java.net.InetSocketAddress

import play.PlayRunHook
import sbt._

object GruntPlayRunHook {
  def apply(baseFolder: File, gruntFile: String, gruntTask: String): PlayRunHook = {
    println("+++ loading GruntPlayRunHook")

    object GruntRunner extends PlayRunHook {
      var gruntProcess: Option[Process] = None

      override def beforeStarted(): Unit = {
        println("+++ GruntRunner#beforeStarted")
      }

      override def afterStarted(addr: InetSocketAddress): Unit = {
        println("+++ GruntRunner#afterStarted")
        gruntProcess = Some(runGruntProcess(baseFolder, gruntFile, gruntTask))
      }

      override def afterStopped(): Unit = {
        println("+++ GruntRunner#afterStopped")

        gruntProcess.map(p => p.destroy())
        gruntProcess = None
      }

      private def runGruntProcess(baseFolder: File, gruntFile: String, gruntTask: String) = {
        //probably linux / unix only

        val p: ProcessBuilder = Process("grunt" :: "--gruntfile=" + gruntFile :: gruntTask :: Nil, baseFolder)

        println(s"+++ Running grunt command: ${p.toString} in ${baseFolder.getPath}")
        p.run
      }
    }

    GruntRunner
  }
}