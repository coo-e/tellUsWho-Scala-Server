import java.net.InetSocketAddress

import play.PlayRunHook
import sbt._

object TestPlayRunHook {
  def apply(): PlayRunHook = {
    println("loading play run hook")

    object Hook extends PlayRunHook {
      override def beforeStarted(): Unit = {
        println("test hook before started")
      }

      override def afterStarted(addr: InetSocketAddress): Unit = {
        println("test hook after started")
      }

      override def afterStopped(): Unit = {
        println("test hook after stopped")
      }
    }

    Hook
  }
}