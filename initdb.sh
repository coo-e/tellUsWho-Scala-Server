
#!/bin/bash
echo "******CREATING DOCKER DATABASE******"
gosu postgres postgres --single <<- EOSQL
CREATE DATABASE docker;
CREATE USER docker;
GRANT ALL PRIVILEGES ON DATABASE docker TO docker;
echo ""
echo "******DOCKER DATABASE CREATED******"
EOSQL
echo "CREATING TABLES"
gosu postgres postgres --single docker <<- EOSQL
CREATE TABLE questions( \
id serial NOT NULL, \
questiontype text, \
versionid integer, \
question text, \
questionsection text, \
        CONSTRAINT questions_id_key UNIQUE (id)) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE questions OWNER TO docker; \
GRANT ALL ON TABLE questions TO docker; \
CREATE TABLE users( \
id serial NOT NULL, \
email text, \
sessauth text, \
starttime timestamp with time zone, \
CONSTRAINT users_email_pkey PRIMARY KEY(email), \
CONSTRAINT users_email_key UNIQUE (email), \
CONSTRAINT users_id_key UNIQUE (id) \
) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE users OWNER TO docker; \
GRANT ALL ON TABLE users TO docker; \
CREATE TABLE basicinfo( \
firstname text, \
lastname text, \
email text, \
smartphone text, \
gender text, \
fakeuser boolean, \
CONSTRAINT basicinfo_pkey PRIMARY KEY (email) \
) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE basicinfo OWNER TO docker; \
GRANT ALL ON TABLE basicinfo TO docker; \
CREATE TABLE responses \
( \
response text, \
useremail text, \
questionid integer, \
id serial, \
CONSTRAINT response_pkey PRIMARY KEY (id), \
CONSTRAINT question_id_fk FOREIGN KEY (questionid) \
REFERENCES questions (id) MATCH SIMPLE \
ON UPDATE NO ACTION ON DELETE NO ACTION, \
CONSTRAINT responses_useremail_fkey FOREIGN KEY (useremail) \
REFERENCES users (email) MATCH SIMPLE \
ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
        ); \
ALTER TABLE responses OWNER TO docker; \
GRANT ALL ON TABLE responses TO docker; \
CREATE INDEX fki_question_id_fk \
  ON responses \
  USING btree \
  (questionid); \
CREATE TABLE demographics \
( \
  id serial, \
  email text, \
  birthdate text, \
  nationality text, \
  hometown text, \
  native_lang text, \
  other_langs text, \
  relationship_status text, \
  current_city text, \
  fakeuser boolean, \
CONSTRAINT demo_pkey PRIMARY KEY (id), \
CONSTRAINT demo_useremail_fkey FOREIGN KEY (email) \
  REFERENCES users (email) MATCH SIMPLE \
  ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
        ); \
ALTER TABLE demographics OWNER TO docker; \
GRANT ALL ON TABLE demographics TO docker; \
CREATE TABLE school_work \
( \
  id serial, \
  studenttype text, \
  email text, \
  major text, \
  campusorgs text, \
  workfield text, \
  volunteerfield text, \
  status text, \
  commuter text, \
  oncampus_activity text, \
  tostudy text, \
  fakeuser boolean, \
  inserttime timestamp with time zone, \
CONSTRAINT school_pkey PRIMARY KEY (id), \
CONSTRAINT school_useremail_fkey FOREIGN KEY (email) \
REFERENCES users (email) MATCH SIMPLE \
ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
        ); \
ALTER TABLE school_work OWNER TO docker; \
GRANT ALL ON TABLE school_work TO docker; \
CREATE TABLE socializing \
( \
  id serial, \
  email text, \
  bewithpeople integer, \
  socialmixing integer, \
  workwithothers integer, \
  people_stim integer, \
  nocontact integer, \
  social_awk integer, \
  stranger_converse_diff integer, \
  stranger_tense integer, \
  dumb_talk integer, \
  nervous_authority integer, \
  party_awk integer, \
  shy_opp_sex integer, \
  inserttime timestamp with time zone, \
CONSTRAINT social_pkey PRIMARY KEY (id), \
CONSTRAINT social_useremail_fkey FOREIGN KEY (email) \
REFERENCES users (email) MATCH SIMPLE \
ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
        ); \
ALTER TABLE socializing OWNER TO docker; \
GRANT ALL ON TABLE socializing TO docker; \
CREATE TABLE interests \
( \
id serial, \
email text, \
interest text, \
learn_about text, \
looking_for text, \
unique_interest text, \
sport_watch text, \
sport_play text, \
games text, \
try text, \
help text, \
expert text, \
beginner text, \
willing_to_teach text, \
fakeuser boolean, \
inserttime timestamp with time zone, \
CONSTRAINT interests_pkey PRIMARY KEY (id), \
CONSTRAINT interests_email_fk FOREIGN KEY (email) \
  REFERENCES users (email) MATCH SIMPLE \
  ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
        WITH ( \
                  OIDS=FALSE \
        ); \
ALTER TABLE interests OWNER TO docker; \
GRANT ALL ON TABLE interests TO docker; \
CREATE TABLE survey_instance \
( \
  id serial, \
  useremail text, \
  surveydate DATE, \
  starttime TIMESTAMP WITH TIME ZONE, \
  endtime TIMESTAMP WITH TIME ZONE, \
  completed boolean, \
  CONSTRAINT survey_instance_pkey PRIMARY KEY (id), \
  CONSTRAINT survey_useremail_fkey FOREIGN KEY (useremail) \
      REFERENCES users (email) MATCH SIMPLE \
      ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
        WITH ( \
                OIDS=FALSE \
        ); \
ALTER TABLE survey_instance OWNER to docker; \
GRANT ALL ON TABLE survey_instance to docker; \
CREATE TABLE passionlevels( \
passid serial, \
userid integer, \
interest text, \
passionlevel integer, \
email text, \
CONSTRAINT plevel_pkey_id PRIMARY KEY (passid), \
CONSTRAINT plevel_demo_id FOREIGN KEY (userid)  \
REFERENCES demographics(id) MATCH SIMPLE, \
CONSTRAINT plevel_id_fk FOREIGN KEY (email) \
REFERENCES basicinfo (email) MATCH SIMPLE \
ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE passionlevels OWNER TO docker; \
GRANT ALL ON TABLE passionlevels TO docker; \
CREATE TABLE contacts \
( \
  id serial, \
  useremail text NOT NULL, \
  name text, \
  pictureurl text, \
  contactemail text, \
  CONSTRAINT contacts_pkey PRIMARY KEY (id), \
  CONSTRAINT contacts_email_fkey FOREIGN KEY (useremail) \
        REFERENCES users(email) MATCH SIMPLE \
        ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE contacts OWNER TO docker; \
GRANT ALL ON TABLE contacts TO docker; \
CREATE INDEX fki_contactemail_fk \
  ON contacts \
  USING btree \
  (contactemail); \
  CREATE TABLE matchdata \
( \
  id serial, \
  touser_id integer, \
  demo_id integer, \
  interest_id integer, \
  userid integer NOT NULL, \
  matchid_day DATE, \
  matchid_num integer, \
  reason integer, \
  matchtype integer NOT NULL, \
  CONSTRAINT matchdata_pkey PRIMARY KEY (id), \
  CONSTRAINT matchdata_touser_fkey FOREIGN KEY (touser_id) \
        REFERENCES users(id) MATCH SIMPLE \
        ON UPDATE NO ACTION ON DELETE NO ACTION, \
  CONSTRAINT matchdata_demo_fkey FOREIGN KEY (demo_id) \
        REFERENCES demographics(id) MATCH SIMPLE \
        ON UPDATE NO ACTION ON DELETE NO ACTION, \
  CONSTRAINT matchdata_interest_fkey FOREIGN KEY (interest_id) \
        REFERENCES interests(id) MATCH SIMPLE \
        ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE matchdata OWNER TO docker; \
GRANT ALL ON TABLE matchdata TO docker; \
CREATE INDEX fki_touserid_fk \
  ON matchdata \
  USING btree \
  (touser_id); \
  CREATE TABLE matchdata_feedback \
( \
  id serial, \
  tomatch_id integer, \
  demo_id integer, \
  interest_id integer, \
  userid integer NOT NULL, \
  crossed_paths text, \
  saved boolean, \
  time_pushed TIMESTAMP WITH TIME ZONE, \
  time_responded TIMESTAMP WITH TIME ZONE, \
  time_saved TIMESTAMP WITH TIME ZONE, \
  response integer, \
  r2m_feedback integer, \
  age_feedback integer, \
  gender_feedback integer, \
  hometown_feedback integer, \
  nationality_feedback integer, \
  major_feedback integer, \
  interest1_feedback integer, \
  interest2_feedback integer, \
  interest3_feedback integer, \
  interest4_feedback integer, \
  interest5_feedback integer, \
  CONSTRAINT matchfeedback_pkey PRIMARY KEY (id), \
  CONSTRAINT matchfeedback_touser_fkey FOREIGN KEY (userid) \
        REFERENCES users(id) MATCH SIMPLE \
        ON UPDATE NO ACTION ON DELETE NO ACTION, \
  CONSTRAINT matchfeeback_demo_fkey FOREIGN KEY (demo_id) \
        REFERENCES demographics(id) MATCH SIMPLE \
        ON UPDATE NO ACTION ON DELETE NO ACTION, \
  CONSTRAINT matchfeedback_interest_fkey FOREIGN KEY (interest_id) \
        REFERENCES interests(id) MATCH SIMPLE \
        ON UPDATE NO ACTION ON DELETE NO ACTION \
) \
WITH ( \
          OIDS=FALSE \
); \
ALTER TABLE matchdata_feedback OWNER TO docker; \
GRANT ALL ON TABLE matchdata_feedback TO docker;
EOSQL
echo "TABLES CREATED"
