/**
 * Created by kingHenry on 6/1/15.
 */

var Reflux = require('Reflux')

    // Each action is like an event channel for one specific event. Actions are called by components.
    // The store is listening to all actions, and the components in turn are listening to the store.
    // Thus the flow is: User interaction -> component calls action -> store reacts and triggers -> components update

var Actions = Reflux.createActions([
        "respondToQuestion",    //called when question is  initially responded to (text filled in, answer choice selected, etc)
        "editResponse",         //called when question response is modified
        "clearResponse"        // called by editing to remove selection or text entry (clear)
    ]);

