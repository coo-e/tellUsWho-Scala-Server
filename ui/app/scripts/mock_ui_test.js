/**
 * Created by Jakeland on 7/2/2015.
 */

var $ = require('jquery');
var _ = require('lodash');
var React = require('react');
var Select = require('react-select');
var Maps, List = require('immutable');
var answerHandler = require('./required_functions/answerHandler');


var textboxController = require('./react_components/textEntry');
var dragAndDrop = require('./react_components/dragAndDrop');
var buttonGroup = require('./react_components/button');
var button = require('./react_components/button');
var textBoxTest = require('./react_components/answerComponent');
var layout = require('./react_components/layout');
var selection = require('./react_components/selectionTypes');
var question = require('./react_components/question');
var answer = require('./react_components/answerComponent');
var dropdown = require('./react_components/answerComponent');

var Survey = require('./react_components/SurveyComponent');
var SubSection = require('./react_components/SubSectionComponent');
var Page = require('./react_components/PageComponent');
var test_survey= require('./mock_front_json/mock_json_test');
