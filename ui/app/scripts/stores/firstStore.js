var Actions = Reflux.createActions([
    "respondToQuestion",    //called when question is  initially responded to (text filled in, answer choice selected, etc)
    "editResponse",         //called when question response is modified
    "clearResponse"        // called by editing to remove selection or text entry (clear)
]);

var Store = Reflux.createStore({

    // Initial setup
    init: function() {

        // Register actions
        this.listenTo(Actions.respondToQuestion, this.onRespondToQuestion);
        this.listenTo(Actions.editResponse, this.onEditResponse);
        this.listenTo(Actions.clearResponse, this.onClearResponse);
    },

    // Callback
    onRespondToQuestion : function(){
        console.log("much response")
    },
    onEditResponse : function(){
        console.log("much editing")
    },
    onClearResponse: function() {
        console.log("such clearing of the response")
    }

});