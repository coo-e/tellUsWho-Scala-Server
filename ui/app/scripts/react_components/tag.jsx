var React = require('react');
var Maps, List = require('immutable');
var _ = require('lodash');
var Select = require('react-select');
var TagTextView = require('./textEntry').TagTextView;
var Textbox = require('./textEntry').TextboxController;
var TextboxView = require('./textEntry').TextboxView;
var ButtonController = require('./button').ButtonController;
var DropDownComponent = require('./answerComponent').DropDownComponent;
var BaseButtonView = require('./button').BaseButtonView;
var BaseButtonComponent = require('./button').BaseButtonComponent;


//testing var//
var testTagData = [];
//end testing var//

//Testing React-Select
function logChange(val) {
    console.log("Selected: " + val);
}
var options = [{value: 'ome', label: 'One'}, {value: 'two', label:'Two'}];

/*
var CustomRenderMultiField = React.createClass({
    displayName: 'CustomRenderMultiField',

    onLabelClick: function onLabelClick(data, event) {
        console.log(data, event);
    },
    renderOption: function renderOption(option) {
        return React.createElement(
            'span',
            { style: { color: option.hex } },
            option.label,
            ' (',
            option.hex,
            ')'
        );
    },
    renderValue: function renderValue(option) {
        return React.createElement(
            'strong',
            { style: { color: option.hex } },
            option.label
        );
    },
    _getCSS: function(){
        var styles ={
            display:'inline-block',
            position:'relative',
            float:'left',
            width:'100%',
            maxWidth:400,
            background:'#00c5fa',
            borderRadius:25,
            paddingLeft:5,
            paddingRight:5
        };
        return styles;
    },
    render: function render() {
        var ops = [{ label: 'Red', value: 'red', hex: '#EC6230' }, { label: 'Green', value: 'green', hex: '#4ED84E' }, { label: 'Blue', value: 'blue', hex: '#6D97E2' }];
        return React.createElement(
            'div',
            null,
            React.createElement(
                'label',
                null,
                this.props.label
            ),
            React.createElement(Select, {
                style:this._getCSS(),
                delimiter: ',',
                multi: true,
                allowCreate: true,
                placeholder: 'Select your favourite(s)',
                options: ops,
                optionRenderer: this.renderOption,
                valueRenderer: this.renderValue,
                onChange: logChange })
        );
    }
});
*/
//End Testing React-Select
//Testing tag component



class TagView extends React.Component{
    constructor(props){
        super(props);
    }
    _getCSS(){
        var styles = {
            display:'inline-block',
            position:'relative',
            float:'left',
            background:'#00c5fa',
            borderRadius:25,
            paddingLeft:5,
            paddingRight:5
        };
        return styles;
    }
    render(){
        return(
            <div key = {this.props.key} style = {this._getCSS()}>{this.props.children}</div>
        )
    }
}

var TagGroup = React.createClass({
    generateTag:function(tag){


            var id = this.props.tags.indexOf(tag);
            return (
                <TagView key={id}>{tag}</TagView>
            );
    },
    render:function(){

        var tags = this.props.tags.map(this.generateTag);
        return(<div>
            {tags}
        </div>);
    }
});
var TagComponent = React.createClass({

    render(){
        return(
            <TagView/>
        );
    }
});


module.exports.TagGroup = TagGroup;
module.exports.TagComponent = TagComponent;
