var React = require('react');
var Maps, List = require('immutable');
var _ = require('lodash');
var Select = require('react-select');

var answerHandler = require('../required_functions/answerHandler').answerHandler;


var TextboxController = React.createClass({
    /*getInitialState: function () {
     return {value: ""};

     },
     handleChange: function (event) {
     this.setState({value: event.target.value});

     },
     */
    renderChildren: function(passedProps) {
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps)
        });
    },
    render: function () {

        var props = {};
        props.handleKeyDown = this.props.handleKeyDown;

        props.value = this.props.value;
        props.handleChange = this.props.handleChange;
        props.ref = this.props.ref;
        return (
            <span>
                    {this.renderChildren(props)}
                    </span>
        );
    }

});

class TextboxView extends React.Component{
    constructor(props){
        super(props);
    }
    getCSS() {

    }


    render() {
        var props = {};
        props.focus =  this.props.focus;
        props.onBlur = this.props.handleBlur;
        props.onFocus = this.props.handleFocus;
        props.style = this.getCSS();
        props.ref = "myInput";
        props.type = "text";
        props.value = this.props.value;
        console.log(props.value)
        props.onKeyDown = this.props.handleKeyDown;
        props.onChange = this.props.handleChange;

        return ( <input {...props}
            />);

    }
}

class TagTextView extends TextboxView{
    getCSS() {
        var styles;

        styles = {
            textDecoration: 'none !important',
            border:0 ,
            borderColor:'transparent !important',
            WebkitAppearance:'none !important',
            outline:'none !important',
            width:'100%'
        };

        return styles;
    }
}




var TextTest = React.createClass({
    render: function () {
        return (
            <div>
                //View!
                <TagTextView/>
                //Controller + View!
                <TextboxController>
                    <TextboxView />
                </TextboxController>
            </div>
        );
    }
});
exports.TextTest = TextTest;
exports.TagTextView = TagTextView;
exports.TextboxController =TextboxController;
exports.TextboxView = TextboxView;
/*exports.ComboBox = ComboBox;*/
//React.render(<DropDown style = {{display:'inline-block',
//float:'left',
//width:'100%',
//position:'relative'}}/>,
//    document.querySelector('.singleSelectTextEntry')
//);
