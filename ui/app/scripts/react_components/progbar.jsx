var React = require('react');
var Maps, List = require('immutable');
var _ = require('lodash');

var ProgBarLabel = React.createClass({
    render : function() {
        return (
            <p>{this.props.text}</p>
        );

    }


});
var ProgBarStatus = React.createClass({

    render: function(){

        return (
            this.props.status
        );
    }

});
var ProgBarItem = React.createClass({
    generateContent: function() {
        return <ProgBarLabel text = {this.props.text}/>;
    },
    generateStatus: function() {
        return <ProgBarStatus status = {this.props.status}/>;

    },

    createCss: function(status) {
        var progStyle;



        var  active = {
            'styles': {

                backgroundColor: '#00EC90',

                borderRadius: '50%',
                paddingTop: 50,
                paddingBottom: 50,
                marginTop: -50,
                marginBottom: -50,
                marginRight: -8,
                marginLeft:-8,
                zIndex: 5

            }
        };


            var  inactive  = {
                'styles' : {
                   float: 'left',
                   display: 'inline-block',
                   backgroundColor: '#0D83EA',
                   paddingLeft: 50,
                   paddingRight: 50,


                   zIndex: 1,
                   position: 'relative'
                }





            };


            var passed = {
                'styles' : {
                    backgroundColor:'#00EC90'
                }
            };



        if(status == 'passed')
            _.merge(inactive, passed);
        if(status == 'active')
            _.merge(inactive, active);

        progStyle = inactive.styles;





        return(progStyle);

    },

    render:function (){
        var content = this.generateContent();

        var styles = this.createCss(this.props.status);

        return (
            <li style = {styles}>
                {content}
            </li>

        );

    }


});


    var ProgBar = React.createClass({
    generateItem: function(item) {
            return <ProgBarItem text={item.text} status={item.status} />
    },
        createCss: function(){
            var ulStyles;

            ulStyles = {
            display: "inline-block",
            paddingTop: 20,
            paddingBottom: 20,
            textAlign: 'center',

            };
            return(ulStyles);
        },
    render: function () {

        var items = this.props.items.map(this.generateItem);
        var styles = this.createCss();
        return (
            <ul class = "progressBarContainer" style = {styles}>
                {items}
            </ul>
        );

    }
});

var ProgBarContainer = React.createClass({
    getProgBar: function (e) {
        return <ProgBar items={e} />
    },
    createCss: function () {
        var progBarStyle = {
            display: "inline-block",
            textAlign: "center",
            width: "100%",


        };
        return (progBarStyle);
    },
    render: function () {
        var theseitems = this.props.items;


        var styles = this.createCss();
        var BarContainer = this.getProgBar(theseitems);
        return (
            <span style = {styles}>{BarContainer}</span>
        )

    }
});




module.exports = React.render(
    <ProgBarContainer items = {prog} />,
    document.getElementById('progressBar')
);