var React = require('react');

var NavBarLink = React.createClass({
    render: function() {
        return (
            <a href={this.props.url}>{this.props.text}</a>
        );
    }
});

var NavBarItem = React.createClass({
    generateLink: function () {
        //Right now we don't need our class but what if we wanted to change the text, add an arrow or something?
        //Single responsibility principles tell us that it our "Item" should not handle this.
        return <NavBarLink url={this.props.url} text={this.props.text}/>;
    },
    generateSubmenu: function () {
        //We generate a simple Navbar (the parent).
        //Spoilers: it takes items as its argument.
        return <NavBar items={this.props.submenu}/>
    },
    generateContent: function () {
        var content = [this.generateLink()];
        if (this.props.submenu) {
            //If there is a submenu in our data for this item
            //We add a generated Submenu to our content
            content.push(this.generateSubmenu());
        }
        return content;
    },
    render: function () {
        var content = this.generateContent();
        return (
            <li>
                {content}
            </li>
        );
    }
});

var NavBar = React.createClass({
    generateItem: function (item) {
        return <NavBarItem text={item.text} url={item.url} submenu={item.submenu}/>
    },
    render: function () {
        var items = this.props.items.map(this.generateItem);
        return (
            <ul className="menu">
                {items}
            </ul>
        );
    }
});

module.exports = React.render(
    <NavBar items={data} />,
    document.querySelector('nav')
);
