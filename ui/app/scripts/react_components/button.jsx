
var React = require('react/addons');
var _=require('lodash');
var Maps, List = require('immutable');


var MultiSelect = require('./selectionTypes').MultiSelect;
var SingleSelect = require('./selectionTypes').SingleSelect;
var HorizontalLayout = require('./layout').HorizontalLayout;

//Test Data, can be deleted once finished
var testData = [
    {
        "text": "Not at all",
        "AnswerKey": "1"
    },
    {
        "text": "Somewhat",

        "AnswerKey": "2"
    },
    {
        "text":  "Moderately",

        "AnswerKey": "3"


    },
    {
        "text": "Very",

        "AnswerKey": "4"
    },
    {
        "text": "Extremely",

        "AnswerKey": "5"
    }
];
var checkTestData = [
    {
        "text": "Not at all",
        "checked":false,
        "AnswerKey": "1"
    },
    {
        "text": "Somewhat",
        "checked":false,
        "AnswerKey": "2"
    },
    {
        "text":  "Moderately",
        "checked":false,
        "AnswerKey": "3"


    },
    {
        "text": "Very",
        "checked":false,
        "AnswerKey": "4"
    },
    {
        "text": "Extremely",
        "checked":false,
        "AnswerKey": "5"
    }
]
//End Test Data

class BaseButtonView extends React.Component {

    constructor(props){
        super(props);
    }
    /*JMC --figure out how you are supposed to add proptypes*/
    _getCSS(checked, hover, pressed){
        var styleTest = {
            position:'relative',
            background:'red',
            width: 20,
            height: 20,
            padding: 30,
            margin: 30,
            float: 'left',
            cursor:'pointer'
        };
        var stylesChecked = {
            background:'green'
        };
        var stylesHover = {
            background:'blue'
        };
        var stylesPressed = {
            background:'orange'
        };
        if(hover == true)
        {
            _.merge(styleTest, stylesHover);
        }
        if (pressed == true){
            _.merge(styleTest, stylesPressed);
        }
        if(checked == true){
            //do this one thing
            _.merge(styleTest, stylesChecked);
        }
        return(styleTest);

    }
    render(){
        this._getCSS = this._getCSS.bind(this);
        return <div {...this.props}
            onMouseUp = {this.props.handleMouseUp}
            onMouseOut = {this.props.handleMouseOut}
            onMouseOver = {this.props.handleMouseOver}
            onMouseDown = {this.props.handleMouseDown}
            onClick= {this.props.handleClick}
            style = {this._getCSS(this.props.checked,this.props.hover,this.props.pressed)}>
            {this.props.content}
        </div>;
    }
}


class DropDownButtonView extends BaseButtonView{
    _getCSS(checked, hover, pressed){
        var styles = {

            position:'relative',
            background:'white',
            display:'inline-block',
            /*JMC -if this doesn't work, check the Width of the parent, and set minWidth*/
            width:'100%',
            height:'auto',
            paddingTop: 5,
            paddingBottom:2,
            float: 'left',
            cursor:'pointer'
        };
        var stylesChecked = {
            background:'blue'
        };
        var stylesHover = {
            background:'blue'
        };
        var stylesPressed = {
            background:'blue'
        };
        if(hover == true)
        {
            _.merge(styles, stylesHover);
        }
        if (pressed == true){
            _.merge(styles, stylesPressed);
        }
        if(checked == true){
            //do this one thing
            _.merge(styles, stylesChecked);
        }
        return(styles);
    }

}


class SecondButtonView extends BaseButtonView{
    _getCSS(checked, hover, pressed){
        var styles = {
            position:'relative',
            background:'red',
            width: 20,
            height: 20,
            padding: 30,
            margin: 30,
            float: 'left',
            cursor:'pointer'
        };
        var stylesChecked = {
            background:'green'
        };
        var stylesHover = {
            background:'blue'
        };
        var stylesPressed = {
            background:'orange'
        };
        if(hover == true)
        {
            _.merge(styles, stylesHover);
        }
        if (pressed == true){
            _.merge(styles, stylesPressed);
        }
        if(checked == true){
            //do this one thing
            _.merge(styles, stylesChecked);
        }
        return(styles);
    }
}



var ButtonController = React.createClass({
    propTypes:{
        handleClick: React.PropTypes.func //tells the Button what it should do when clicked. Passed down from parent atribute or assigned on creation.
    },
    selectHandler: function(event) {
        if (typeof this.props.handleClick === 'function') {
            //ev is used to handle the click event
            this.props.handleClick(this.props.tag);
        }
    },


    renderChildren: function(passedProps) {
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps)
        });
    },
    render: function() {

        var props = {};

        props.AnswerKey =  this.props.AnswerKey;
        props.content = this.props.content;
        props.checked = this.props.checked;
        props.pressed = this.props.pressed;
        props.hover = this.props.hover;
        props.handleClick = this.selectHandler;
        props.handleMouseDown = this.props.handleMouseDown;
        props.handleMouseUp = this.props.handleMouseUp;
        props.handleMouseOut = this.props.handleMouseOut;
        props.handleMouseOver = this.props.handleMouseOver;

        return(
            <span{ ...props}>
                {this.renderChildren(props)}
                </span>
        );
    }
});

class BaseButtonComponent extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            checked:false,
            hover:false,
            pressed:false
        }

    }
    /*handleChange: function(currVal){
     },*/
    handleClick(){
        if (typeof this.props.handleClick === 'function') {
            //ev is used to handle the click event
            this.props.handleClick(this.props.AnswerKey);
        }
}
    mouseOver(){
    this.setState({hover: true});
}
    mouseOut(){
    this.setState({hover:false});
    this.setState({pressed:false});
}
    mouseUp(){
    this.setState({pressed:false});
}
    mouseDown(){
    this.setState({pressed:true});
}
    getView(content){
        return(
            <BaseButtonView>
                {content}
                </BaseButtonView>
        )
    }
    render(){
        this.getView = this.getView.bind(this);
        this.mouseDown = this.mouseDown.bind(this);
        this.mouseOver = this.mouseOver.bind(this);
        this.mouseOut = this.mouseOut.bind(this);
        this.mouseUp = this.mouseUp.bind(this);
        this.handleClick = this.handleClick.bind(this);
        var props = {};
        props.content = this.props.text;
        if(this.props.text == undefined){
            props.content = this.props.content;
        }
        props.AnswerKey = this.props.AnswerKey;
        props.handleMouseDown = this.mouseDown;
        props.handleMouseUp = this.mouseUp;
        props.handleClick = this.handleClick;
        props.handleMouseOut = this.mouseOut;
        props.handleMouseOver = this.mouseOver;
        props.checked = this.state.checked;
        props.hover = this.state.hover;
        props.pressed = this.state.pressed;
        return(
            <ButtonController  {...props} >
                {this.getView(props.content)}
            </ButtonController>
        )
    }
}
class CheckBoxComponent extends BaseButtonComponent{
    handleClick(){
        if(this.state.checked == true)
        this.setState({checked:false});
        else{
            this.setState({checked:true});
        }
    var checkTest = this.state.checked;
    this.props.handleClick(this.props.AnswerKey);
}
}


class DropDownButton extends BaseButtonComponent{
    getView(content){
        return(
            <DropDownButtonView>
                {content}
                </DropDownButtonView>
        );

    }
}
/*
This is purely used for Testing purposes
 */



/*
End Testing
 */
//Exports
module.exports.CheckBoxComponent = CheckBoxComponent;
module.exports.ButtonController = ButtonController;
module.exports.BaseButtonView = BaseButtonView;
module.exports.DropDownButton = DropDownButton;
module.exports.BaseButtonComponent = BaseButtonComponent;

