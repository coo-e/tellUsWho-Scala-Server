
var React = require('react/addons');
var _=require('lodash');
var Maps, List = require('immutable');

var BaseButtonView = require('./button').BaseButtonView;
var DropDownButton = require('./button').DropDownButton;
var HorizontalLayout = require('./layout').HorizontalLayout;
var VerticalGroupLayout = require('./layout').VerticalLayout;

//Test Data, can be deleted once finished
var testData = [
    {
        "text": "Not at all",
        "AnswerKey": "1"
    },
    {
        "text": "Somewhat",

        "AnswerKey": "2"
    },
    {
        "text":  "Moderately",

        "AnswerKey": "3"


    },
    {
        "text": "Very",

        "AnswerKey": "4"
    },
    {
        "text": "Extremely",

        "AnswerKey": "5"
    }
];
//EndTestData;

var DropDownController = React.createClass({
    getInitialState:function(){
        return {
            isVisible: false
        }
    },
    handleClick:function(event){
        this.setState({isVisible:false});
    },
    handleKeyDown: function(event){
        var ENTER = 13;

        if (event.which == ENTER) {
            this.handleClick(event);

        }
        else{
            this.setState({isVisible:true});
        }

    },
    renderChildren:function(passedProps){
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps);
        });
    },
    render:function(){
        var props = {};
        props.isTyping = this.props.isTyping;
        props.isVisible = this.state.isVisible;

        return (
            <span>
                {this.renderChildren(props)}
                </span>
        );

    }



});

var DropDownView = React.createClass({
    getCSS: function(isVisible){

        var styles = {
            position:'relative',
            background:'white',
            width:'100%',
            height:'auto',
            maxHeight:300,
            display:'inline-block'
        };
        var invisible = {
            display:'none'
        };

        if(isVisible == false){
            _.merge(styles,invisible);
        };


    },
    renderChildren:function(passedProps){
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps)
        });
    },
    render:function(){
        var props = {};
        props.isTyping = this.props.isTyping;
        props.isVisible = this.props.isVisible;

        return(
            <div style = {this.getCSS(this.props.isVisible)} ref = {this.props.ref}>
                {this.renderChildren(props)}
                </div>
        )
    }
});




module.exports.DropDownController = DropDownController;
module.exports.DropDownView = DropDownView;