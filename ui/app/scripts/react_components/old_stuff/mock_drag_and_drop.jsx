var React = require('react');
var Maps, List = require('immutable');
var _ = require('lodash');


var DragAndDropView = React.createClass({
    getCSS: function(){
        var styles;
        styles = {
            background:'blue',
            display:'inline-block',
            position: 'relative',
            minWidth:175,
            minHeight:175,
            borderRadius:25,
            cursor:'pointer'


        };
        return(
            styles
        );
    },
    render: function(){
        return(
        <div style = {this.getCSS()}/>
        );
    }

});

var DragAndDropController = React.createClass({
        getInitialState: function(){
        return({
            pressed:false,
            hover:false,
            dragable:true,
            droppable:false,
            savedValue: ''
        });
    },
    render: function(){
        return(
            <DragAndDropView
                pressed = {this.state.pressed}
                hover = {this.state.hover}
                dragable = {this.state.dragable}
                droppable = {this.state.droppable}
                savedValue = {this.state.savedValue}
                />
        )
    }
});


/*
React.render(<DragAndDropView />,
document.getElementById("dragAndDropView"));
    */