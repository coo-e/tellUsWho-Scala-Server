
var React = require('react/addons');
var _=require('lodash');
var Maps, List = require('immutable');

var test_question1 = require('../mock_front_json/mock_json_test').test_question1;
var test_question2 = require('../mock_front_json/mock_json_test').test_question2;
var test_question3 = require('../mock_front_json/mock_json_test').test_question3;

var Text_entry = require('./textEntry').TextboxController;
var TextboxView = require('./textEntry').TextboxView;

var CheckBoxGroup = require('./answerComponent.jsx').CheckBoxGroup;
var testData = [
    {
        "text": "Not at all",
        "AnswerKey": "1"
    },
    {
        "text": "Somewhat",

        "AnswerKey": "2"
    },
    {
        "text":  "Moderately",

        "AnswerKey": "3"


    },
    {
        "text": "Very",

        "AnswerKey": "4"
    },
    {
        "text": "Extremely",

        "AnswerKey": "5"
    }
];
var QuestionController = React.createClass({
    getQuestionType: function(parameters) {

        var props = {};
        for (var key in parameters){
            props[key] = parameters[key];

        }
        return props;
    },
    renderChildren:function(passedProps){
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps)
        });
    },
    render:function(){
        var props = this.getQuestionType(this.props.parameters);
        return(
            <div>
                {this.renderChildren(props)}
            </div>
        )
    }


});

var QuestionView = React.createClass({
    getCSS:function(){
        return(
        {display:'inline-block',
         position:'relative'}
        )
    },
    getChild:function(){
        return(
         //something.
            console.log("welp")
        );
    },
    render:function(){
        return(<div style = {this.getCSS()}>
                <div style = {this.getCSS()}>{this.props.text}</div>
            {React.createElement(Text_entry, this.props, <TextboxView/>)}
            </div>
        )
    }
});

var QuestionComponent = React.createClass({
    render:function(){
        return(
            <QuestionController parameters = {this.props.parameters}f>
                <QuestionView/>
            </QuestionController>

            )
    }
});

React.render(<QuestionComponent parameters = {test_question1}/>,
    document.getElementById("first_question_JSON"));