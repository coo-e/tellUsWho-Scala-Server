var React = require('react');
var Maps, List = require('immutable');
var _ = require('lodash');


    var TextboxController = React.createClass({
        /*getInitialState: function () {
            return {value: ""};

        },
        handleChange: function (event) {
            this.setState({value: event.target.value});

        },
        */
        renderChildren: function(passedProps) {
            return React.Children.map(this.props.children, function (child) {
                return React.cloneElement(child, passedProps)
            });
        },
        render: function () {

            var props = {};
            props.handleKeyDown = this.props.handleKeyDown;

            props.value = this.props.value;
            props.onChange = this.props.handleChange;
            props.ref = this.props.ref;
            return (
                <span>
                    {this.renderChildren(props)}
                    </span>
            );
        }

    });

class TextboxView extends React.Component{
    getCSS() {

    }


    render() {

        return ( <input ref = "MyInput"
                        type="text"

                        focus = {this.props.focus}
                        onBlur = {this.props.handleBlur}
                        onFocus ={this.props.handleFocus}
                        style = {this.getCSS()}
                        value={this.props.value}
                        onKeyDown = {this.props.handleKeyDown}
                        onChange={this.props.onChange}/>);

    }
}

class TagTextView extends TextboxView{
    getCSS() {
        var styles;

        styles = {
            textDecoration: 'none !important',
            border:0 ,
            borderColor:'transparent !important',
            WebkitAppearance:'none !important',
            outline:'none !important',
            width:'100%'
        };

        return styles;
    }
}




var TextTest = React.createClass({
    render: function () {
        return (
            <div>
                //View!
                <TagTextView value="this is a test"/>
                //Controller + View!
                <TextboxController>
                    <TextboxView />
                    </TextboxController>
            </div>
        );
    }
});
exports.TagTextView = TagTextView;
exports.TextboxController =TextboxController;
exports.TextboxView = TextboxView;



React.render(<TextTest />,
    document.querySelector('.singleSelectTextEntry')
);