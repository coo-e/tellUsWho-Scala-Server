//currently used  to test ways of combining components in a manner known as "composite components"
//successful test of what is getting returned
var React = require('react');
var Maps, List = require('immutable');
var _ = require('lodash');

var TagTextView = require('./textEntry').TagTextView;
var Textbox = require('./textEntry').TextboxController;
var TextboxView = require('./textEntry').TextboxView;
var ButtonController = require('./button').ButtonController;
var DropDownComponent = require('./dropDown').DropDownComponent;
var BaseButtonView = require('./button').BaseButtonView;
var BaseButtonComponent = require('./button').BaseButtonComponent;
var testTagData = ['test', 'yah'];


var TextButtonComponent = React.createClass({
    getInitialState: function(){
        return{test: "This is going to be updated!",
        tag:'will be updated!',
        value:'',
        isFocused:false}
    },
    handleChange:function(event){
        this.setState({value: event.target.value});
    },
    handleFocus:function(){
        React.findDOMNode(this.refs.myInput).focus();
        this.setState({isFocused:true});
    },
    handleBlur:function(){
        this.setState({isFocused:false});
    },
    handleKeyDown: function(event){
        var ENTER = 13;
        if(this.state.isFocused == true) {
            if (event.which == ENTER) {
                this.handleClick(event);

            }
        }

    },
    handleClick: function(){

        var textVal =  React.findDOMNode(this.refs.myInput).value;


        this.setState({tag:textVal});

    },

    getCSS: function(){
        var styles;
        styles = {
            display: 'inline-block',
            padding:  30,
            background: '#ff0000'
        };
        return styles;
    },
    render: function(){

        return(

            <span style = {this.getCSS()}>
                <div>{this.state.tag}</div>
                <Textbox handleKeyDown = {this.handleKeyDown}
                         value = {this.state.value}
                         handleChange = {this.handleChange}
                         handleFocus = {this.handleFocus}
                         handleBlur = {this.handleBlur}>
                    <TextboxView ref = "myInput"/>
                    </Textbox>
                <BaseButtonComponent ref = "myButton"  content = "Add Item" handleClick = {this.handleClick}>

                </BaseButtonComponent>

                </span>
        );

    }
});


class TagView extends React.Component{
    constructor(props){
        super(props);
    }
    _getCSS(){
        var styles = {
            display:'inline-block',
            position:'relative',
            float:'left',
            background:'#00c5fa',
            borderRadius:25,
            paddingLeft:5,
            paddingRight:5
        };
        return styles;
    }
    render(){
        return(
            <div style = {this._getCSS()}>{this.props.children}</div>
        )
    }
}

var TagGroup = React.createClass({
    generateTag:function(tag){
        return(
            <TagView>{tag}</TagView>
        );
    },
    render:function(){
        var tags = this.props.tags.map(this.generateTag);
       return(<div>
       {tags}
       </div>);
    }
});
var TagComponent = React.createClass({

    render(){
        return(
        <TagView/>
        );
    }
});

var TextAreaTags = React.createClass({
    getInitialState: function(){
        return{tag: "should update now",
            value:"",

        isFocused: false
        }
    },
    handleChange:function(event){

            this.setState({value: event.target.value});


    },
    handleClick: function(event){


        var tagVal = React.findDOMNode(this.refs.myInput).value;
        testTagData.push(tagVal);

        this.setState({tag:tagVal});

        this.setState({value:""});
    },

    handleFocus:function(){
            React.findDOMNode(this.refs.myInput).focus();
            this.setState({isFocused:true});
    },
    handleBlur:function(){
        this.setState({isFocused:false});
    },
    handleKeyDown: function(event){
       var ENTER = 13;
        if(this.state.isFocused == true) {
            if (event.which == ENTER) {
                this.handleClick(event);

            }
        }

    },
    getCSS: function(){
        var styles;
        styles = {
            display: 'inline-block',
            padding:  30,
            background: '#ff0000',
            marginTop: 20
        };
        return styles;
    },


    render: function(){

        return(

            <div  style = {this.getCSS()} onClick = {this.handleFocus} >

                <TagGroup tags = {testTagData} />
                <span>
                <Textbox
                    handleKeyDown = {this.handleKeyDown}
                         value = {this.state.value}
                         handleChange = {this.handleChange}
                         handleFocus = {this.handleFocus}
                        handleBlur = {this.handleBlur}>
                        <TagTextView ref = "myInput"/>
                            </Textbox>
                        <DropDownComponent/>
                    </span>
                <BaseButtonComponent ref = "myButton"  content = "Add Item" handleClick = {this.handleClick}>

                    </BaseButtonComponent>

                </div>
        );

    }
});

module.exports.TagGroup = TagGroup;
module.exports.TagComponent = TagComponent;
