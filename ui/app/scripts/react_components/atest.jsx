/*jshint unused:false*/
var React = require('react');
var Happy = require('./happy');

module.exports = function() {
  React.render(
    <Happy message="Test"/>,
    document.getElementById("content")
  );
};

