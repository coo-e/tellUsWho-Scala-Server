var React = require('react');
var _ = require('lodash');
var Select = require('react-select');

var answerHandler = require('../required_functions/answerHandler').answerHandler;

var HorizontalLayout = require('./layout').HorizontalLayout;
var VerticalLayout = require('./layout').VerticalLayout;
var CheckBoxComponent = require('./button').CheckBoxComponent;
var BaseButtonComponent = require('./button').BaseButtonComponent;
var SingleSelect = require('./selectionTypes').SingleSelect;
var MultiSelect = require('./selectionTypes').MultiSelect;

var Textbox = require('./textEntry').TextboxController;
var TextboxView = require('./textEntry').TextboxView;
var TagGroup = require('./tag').TagGroup;
var TagTextView = require('./textEntry').TagTextView;



var testTagData = [];

var testData = [
    {
        "text": "Not at all",
        "AnswerKey": "1"
    },
    {
        "text": "Somewhat",

        "AnswerKey": "2"
    },
    {
        "text":  "Moderately",

        "AnswerKey": "3"


    },
    {
        "text": "Very",

        "AnswerKey": "4"
    },
    {
        "text": "Extremely",

        "AnswerKey": "5"
    }
];


var ComboSearchEntry = React.createClass({
    logChange: function(){
        var answer = [];
        for(var key in arguments) {
            if (arguments.hasOwnProperty(key)) {
                if(typeof(arguments[key])!== "object") {
                    answer.push(arguments[key]);
                }
            }

        }

        answerHandler(this.props.surveyId,answer)
    },
    onLabelClick: function(data, event) {
        console.log(data,event);
    },
    getInputStyle:function() {
        var styles ={
        };
        return styles;
    },
    getOptionStyle:function(){
        var styles = {

        };
        return styles;
    },
    renderOption: function(option) {
        return <span style={this.getOptionStyle()}>{option.label}</span>;
    },
    renderValue: function(option) {
        return <span style = {{color: 'blue'}}>{option.label}</span>
    },
    getOptions: function(options){
        var formatOptions =[];
        for(var key in options){
            if(options.hasOwnProperty(key)) {
                formatOptions.push({label: options[key], value: options[key]});
            }
        }
        return(formatOptions);
    },
    render: function(){
        var ops = this.getOptions(this.props.answers);
        return(
            <div>
                <label>{this.props.text}</label>
                <Select
                    clearValueText = "x"
                    multi = {true}

                    allowCreate={true}
                    options = {ops}
                    optionRenderer = {this.renderOption}
                    valueRenderer={this.renderValue}
                    onChange={this.logChange}/>

            </div>
        );
    }
});

var DropDown = React.createClass({
    logChange: function(){
        var answer = [];
        for(var key in arguments) {
            if (arguments.hasOwnProperty(key)) {
                if(typeof(arguments[key])!== "object") {
                    answer.push(arguments[key]);
                }
            }

        }

        answerHandler(this.props.surveyId,answer)
    },
    onLabelClick: function(data, event) {
        console.log(data,event);
    },
    getInputStyle:function() {
        var styles ={
        };
        return styles;
    },
    getOptionStyle:function(){
        var styles = {

        };
        return styles;
    },
    renderOption: function(option) {
        return <span style={this.getOptionStyle()}>{option.label}</span>;
    },
    renderValue: function(option) {
        return <span style = {{color: 'blue'}}>{option.label}</span>
    },
    getOptions: function(options){
        var formatOptions =[];
        for(var key in options){
            if(options.hasOwnProperty(key)) {
                formatOptions.push({label: options[key], value: options[key]});
            }
        }
        return(formatOptions);
    },
    render: function(){
        var ops = this.getOptions(this.props.answers);
        return(
            <div>
                <label>{this.props.text}</label>
                <Select
                    clearValueText = "x"
                    multi = {false}

                    allowCreate={false}
                    options = {ops}
                    optionRenderer = {this.renderOption}
                    valueRenderer={this.renderValue}
                    onChange={this.logChange}/>

            </div>
        );
    }
});

var CheckBoxGroup = React.createClass({
    logChange: function(args){
        var answer = [];
        for(var key in args) {
            if (args.hasOwnProperty(key)) {
                if(typeof(args[key])!== "object") {
                    answer.push(args[key]);
                }
            }

        }

        answerHandler(this.props.surveyId,answer)
    },
    getInitialState: function(){
        return{
            returnedVal:[-1]
        }
    },
    setReturnedVal: function(val){
        this.logChange(val);
        this.setState({returnedVal:val});

    },
    render:function(){
        return(
            <MultiSelect items = {this.props.items} setReturnedVal = {this.setReturnedVal}>
                <HorizontalLayout>
                    <CheckBoxComponent/>
                </HorizontalLayout>
            </MultiSelect>
        );
    }

});

var RadioGroup = React.createClass({
    getInitialState: function(){
        return{
            val:[-1],
            returnedVal:[-1]
        }
    },
    setReturnedVal: function(val){
        this.setState({returnedVal:val});
    },
    render:function(){
        return(
            <SingleSelect items = {this.props.items} setReturnedVal = {this.setReturnedVal}>
                <HorizontalLayout>
                    <BaseButtonComponent/>
                </HorizontalLayout>
            </SingleSelect>
        )
    }
});
//JMC THis doesnt work yet, replacing with react-select soon

var TextEntry = React.createClass({
    getInitialState: function(){
        return{
            value:"",
            saveVal:[],
            isFocused:false
        }
    },
    handleChange:function(event){

        this.setState({value:event.target.value});
        this.setState({saveVal:[this.state.value]});
    },
    handleFocus:function(event){
        React.findDOMNode(this.refs.myInput).focus();
        this.setState({isFocused:true});
    },
    handleBlur:function(){
        this.setState({isFocused:false});
    },
    render: function() {
        return (
            <span>
            <div>{this.props.text}here's where the question should go</div>
            <Textbox
                handleKeyDown={this.handleKeyDown}
                value={this.state.value}
                handleChange={this.handleChange}
                handleFocus={this.handleFocus}
                handleBlur={this.handleBlur}>
                <TextboxView ref="myInput"/>
            </Textbox>
                </span>
        )
    }
});








//JMC = TESTING SOME STUFF
//Composite Components, components that are made of two or more components. Will probably move out to a seperate file;

var TextAreaTags = React.createClass({
    getInitialState: function(){
        return{tags: testTagData,
            value:"",
            isFocused: false
        }
    },
    handleChange:function(event){
        this.setState({value: event.target.value});
        answerHandler()
    },
    handleClick: function(event){
        var tagVal = React.findDOMNode(this.refs.myInput).value;
        if(testTagData.indexOf(tagVal) === -1) {
            testTagData.push(tagVal);
        }
        this.setState({value:""});
    },

    handleFocus:function(){
        React.findDOMNode(this.refs.myInput).focus();
        this.setState({isFocused:true});
    },
    handleBlur:function(){
        this.setState({isFocused:false});
    },
    handleKeyDown: function(event){
        var ENTER = 13;


        if (event.which == ENTER) {

            this.handleClick(event);

        }


    },
    getCSS: function(){
        var styles;
        styles = {
            display: 'inline-block',
            padding:  30,
            background: '#ff0000',
            marginTop: 20
        };
        return styles;
    },


    render: function(){
        return(

            <div  style = {this.getCSS()} onClick = {this.handleFocus} >

                <TagGroup tags = {this.state.tags} />
                <span>
                <Textbox
                    handleKeyDown = {this.handleKeyDown}
                    value = {this.state.value}
                    handleChange = {this.handleChange}
                    handleFocus = {this.handleFocus}
                    handleBlur = {this.handleBlur}>
                    <TagTextView ref ="myInput"/>
                </Textbox>
                    {/*<DropDownComponent items = {this.props.items}/>*/}
                    </span>
                <BaseButtonComponent ref = "myButton"  content = "Add Item" handleClick = {this.handleClick}>

                </BaseButtonComponent>

            </div>
        );

    }
});


var TagTextComponent = React.createClass({
    render: function(){
        return(
        <Textbox
            handleKeyDown ={this.props.handleKeyDown}
            value = {this.props.value}
            handleChange = {this.props.handleChange}
            handleFocus = {this.props.handleFocus}
            handleBlur = {this.props.handleBlur}
            >
                <TagTextView ref = "myInput"/>
            </Textbox>

    );
    }
});

var TextButtonComponent = React.createClass({
    getInitialState: function(){
        return{saveVal:[],
            tag:'will be updated!',
            value:'',
            isFocused:false}
    },
    handleChange:function(event){

        this.setState({value: event.target.value});
    },
    handleFocus:function(){
        React.findDOMNode(this.refs.myInput).focus();
        this.setState({isFocused:true});
    },
    handleBlur:function(){
        this.setState({isFocused:false});
    },
    handleKeyDown: function(event){
        var ENTER = 13;
        if(this.state.isFocused == true) {
            if (event.which == ENTER) {
                this.handleClick(event);
            }
        }
    },
    handleClick: function(){
        var textVal =  React.findDOMNode(this.refs.myInput).value;
        this.setState({tag:textVal});
        this.setState({saveVal:[this.state.tag]});
    },
    getCSS: function(){
        var styles;
        styles = {
            display: 'inline-block',
            padding:  30,
            background: '#ff0000'
        };
        return styles;
    },
    render: function(){
        return(
            <span style = {this.getCSS()}>
                <div>{this.state.tag}</div>

               <Textbox handleKeyDown = {this.handleKeyDown}
                         value = {this.state.value}
                         handleChange = {this.handleChange}
                         handleFocus = {this.handleFocus}
                         handleBlur = {this.handleBlur}>
                    <TextboxView ref = "myInput"/>
                </Textbox>
                <BaseButtonComponent ref = "myButton"  content = "Add Item" handleClick = {this.handleClick}>
                </BaseButtonComponent>
                </span>
        );
    }
});


var JakeTest = React.createClass({
    render(){
        return(
           <CheckBoxGroup items = {testData} surveyId = "testing stuff"/>
        )
    }
});



React.render(<JakeTest/>,
    document.getElementById("buttonTest"));
/*
module.exports.DropDownComponent = DropDownComponent;
*/
module.exports.TextEntry = TextEntry;
module.exports.DropDown = DropDown;
module.exports.RadioGroup = RadioGroup;
module.exports.CheckBoxGroup = CheckBoxGroup;
module.exports.ComboSearchEntry = ComboSearchEntry;



React.render(<TextAreaTags items = {testData}/>,
    document.getElementById("testArea"));
