var React = require('react');
var forms = require('newforms')
//var Signup;

var SignupForm = forms.Form.extend({
    username: forms.CharField(),
    email: forms.EmailField(),
    password: forms.CharField({widget: forms.PasswordInput}),
    confirmPassword: forms.CharField({widget: forms.PasswordInput}),
    acceptTerms: forms.BooleanField({required: true})
})

var TestForm =  React.createClass({
    render: function() {
    return <form onSubmit={this._onSubmit}>
<forms.RenderForm form={SignupForm} ref="signupForm"/>
    <button>Sign Up</button>
</form>
},
propTypes: {
    onSignup: React.PropTypes.func.isRequired
},

_onSubmit: function(e) {
    e.preventDefault()

    var form = this.refs.signupForm.getForm()
    var isValid = form.validate()
    if (isValid) {
        this.props.onSignup(form.cleanedData)
    }
},

clean: function() {
    if (this.cleanedData.password &&
        this.cleanedData.confirmPassword &&
        this.cleanedData.password != this.cleanedData.confirmPassword) {
        throw forms.ValidationError('Passwords do not match.')
    }
}
})

module.exports=function(){React.render(<TestForm />,
    document.getElementById('login-page'));
}