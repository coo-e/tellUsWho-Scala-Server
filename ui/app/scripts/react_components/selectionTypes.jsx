
var React = require('react/addons');
var _=require('lodash');
var Maps, List = require('immutable');
var HorizontalLayout = require('./layout').HorizontalLayout;


var SingleSelectControl = React.createClass({
    getInitialState: function(){
        return {
            savedVal : ''
        }
    },
    handleGroup: function(tag){
        this.setState({savedVal: tag});
    },
    handleClick: function(clickedButton)
    {
        this.handleGroup(clickedButton)
    },
    renderChildren: function(passedProps){
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps)
        });
    },
    render: function(){
        var props = {};
        props.handleClick = this.handleClick;
        props.names = this.props.names;
        props.selectedVal= this.state.savedVal;
        props.items = this.props.items;
        return(
            <span>
                {this.renderChildren(props)}
                </span>
        )
    }
});


var MultiSelectControl= React.createClass({
    getInitialState: function(){
        return(
        {savedVal:[]}
        );

    },
    handleGroup: function(tag){
        var saveVal;
        var setReturnedVal = this.props.setReturnedVal;
        var checkArray = this.state.savedVal;
        for (var key in checkArray){
            if (checkArray[key] == tag)
            {
                checkArray.splice(key, 1);
                saveVal = -1;
            }
            else{
                saveVal = key;
            }
        }
        if(saveVal != -1) {
            checkArray.push(tag);
            this.setState({savedVal: checkArray});
        }

        setReturnedVal(checkArray);
    },
    handleClick: function(clickedButton)
    {

        this.handleGroup(clickedButton);

    },
    renderChildren: function(passedProps){
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps)
        });
    },
    render: function(){
        var props = {};
       props.handleClick = this.handleClick;

            props.names = this.props.names;
            props.selectedVal= this.state.savedVal;
            props.items = this.props.items;
            return(
            <span>
                {this.renderChildren(props)}
                </span>
            )


    }


});

module.exports.SingleSelect = SingleSelectControl;
module.exports.MultiSelect = MultiSelectControl;
