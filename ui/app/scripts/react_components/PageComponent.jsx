var React = require('react');
var _=require('lodash');
var Select = require('react-select');

var test_survey= require('../mock_front_json/mock_json_test').test_survey;
var componentRef = require('../../fakedata/ComponentArray').componentRef;




var Page = React.createClass({

    renderChildren: function (passedProps) {
        var myQuestionGroup = passedProps.children;

        return(myQuestionGroup.map(function(group){
            var props = {};
                props = group;
                props.componentRef = passedProps.componentRef;
            var Group = props.componentRef[group.type];

            if(group.type == "ComboSearchEntry")
            console.log(group.type);
            console.log(Group);
            //JMC remove this after updating questions.
            if(Group !== undefined) {
                return (
                    <Group id={group.surveyId} key={group.surveyId} {...props}/>
                )
            }
            else{
                console.log("I'm Undefined!");
                return(<div id = {group.surveyId} key = {group.surveyId}>Here's the undefined Div</div>)
            }

        }));


    },
    render: function () {
        var props = this.props.passedProps;
        props.componentRef = this.props.componentRef;
        console.log(props);
        return (
            <div id={this.props.passedProps.surveyId} key={this.props.passedProps.surveyId}>
                {this.renderChildren(props)}
                </div>


        )
    }
});

module.exports.Page = Page;




