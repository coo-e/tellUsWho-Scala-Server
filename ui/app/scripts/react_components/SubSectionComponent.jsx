var React =require('react');
var _ = require('lodash');
var Select = require('react-select');


var componentRef = require('../../fakedata/ComponentArray').componentRef;
var test_survey= require('../mock_front_json/mock_json_test').test_survey;

var SubSection = React.createClass({
    renderChildren: function(){
        var myChildren = this.props.passedProps.children;
        console.log(this.props.passedProps.children);
        var currPage = this.props.currPage;
        var props = {};

        props.passedProps= myChildren[currPage];
        props.currPage = this.props.currPage;
        props.componentRef = this.props.componentRef;
        var MyChild = this.props.componentRef[myChildren[currPage].type];
        console.log(MyChild);
        return(
        <MyChild {...props}/>

        )
    },
    render: function(){
        var props ={};
        props.id = this.props.passedProps.surveyId;

        console.log(props.id + "woop");
        return(<div id = {props.id}>{this.renderChildren()}</div>)
    }
});

module.exports.SubSection = SubSection;