var React = require('react');
var _= require('lodash');
var Map, List = require('immutable');

var HorizontalLayout = React.createClass({
    propTypes:{
        handleClick: React.PropTypes.func,
        items: React.PropTypes.array //an array of objects to be laid out horizontally , for example buttons, tags, etc.
    },
    renderChildren: function(passedProps) {
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps)
        });
    },
    generateItem: function(item){

        var props =  {};


        for (var key in item){
            props[key] = item[key];
        }
        for (var key in this.props)
        {
            if(key != "items") //
                props[key] = this.props[key];

        }



        return(

            <span>
                {this.renderChildren(props)}
                </span>

        )
    },
    createCSS: function(){
        var styles;
        styles = {
            position:'relative',
            float:'left',
            display:'block',
            paddingTop: 20,
            margin:'0 auto',
            maxWidth:960,
            width:'100%'

        };
        return styles;
    },
    render:function() {


        var items = this.props.items.map(this.generateItem);
        return (
            <div style = {this.createCSS()}>
                {items}
            </div>
        );
    }
});

var VerticalGroupLayout = React.createClass({
    propTypes:{
        handleClick: React.PropTypes.func,
        items: React.PropTypes.array //an array of objects which are to be laid out horizontally, such as buttons, tags, etc.
    },
    renderChildren: function(passedProps) {
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps)
        });
    },
    generateItem: function(item){

        var props =  {};


        for (var key in item){
            props[key] = item[key];
        }
        for (var key in this.props)
        {
            if(key != "items") //
                props[key] = this.props[key];

        }



        return(

            <li>
                {this.renderChildren(props)}
                </li>

        )
    },
    createCSS: function(){
        var styles;
        styles = {
            position:'relative',
            float:'left',
            display:'block',
            margin:0,
            padding:0,
            maxWidth:960,
            width:'100%',
            listStyleType:'none'

        };
        return styles;
    },
    render:function() {


        var items = this.props.items.map(this.generateItem);
        return (
            <ul style = {this.createCSS()}>
                {items}
            </ul>
        );
    }
});
module.exports.VerticalLayout = VerticalGroupLayout;
module.exports.HorizontalLayout = HorizontalLayout;