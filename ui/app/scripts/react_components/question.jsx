
var React = require('react/addons');
var _=require('lodash');
var Maps, List = require('immutable');

var test_question1 = require('../mock_front_json/mock_json_test').test_question1;
var test_question2 = require('../mock_front_json/mock_json_test').test_question2;
var test_question3 = require('../mock_front_json/mock_json_test').test_question3;


var TextEntry = require('./textEntry').TextboxController;
var TextboxView = require('./textEntry').TextboxView;

var CheckBoxGroup = require('./answerComponent.jsx').CheckBoxGroup;

var testData = [
    {
        "text": "Not at all",
        "AnswerKey": "1"
    },
    {
        "text": "Somewhat",

        "AnswerKey": "2"
    },
    {
        "text":  "Moderately",

        "AnswerKey": "3"


    },
    {
        "text": "Very",

        "AnswerKey": "4"
    },
    {
        "text": "Extremely",

        "AnswerKey": "5"
    }
];
var ConditionalDeclineQuestionGroup = React.createClass({
    renderChildren: function (passedProps) {
        return(passedProps.map(function(question){
            var AnswerType = passedProps.componentRef[question.type];
            console.log(question);
            console.log(AnswerType);

            console.log(passedProps);
            var props = question;
            props.componentRef = passedProps.componentRef;
            if(AnswerType  !== undefined) {
                return (
                    <div key = {question.surveyId}>
                        <div>{question.text}</div>
                        <AnswerType {...props} id = {question.surveyId}/>
                    </div>)
            }
            else{
                return(<div key = {question.surveyId}>
                    {question.text}
                    {question.surveyId}"Undifined!"</div>)
            }
        }));


    },
    render: function () {
        var props = {};
        props = this.props.children;
        props.componentRef = this.props.componentRef;

        console.log("Here's the QuestionGroup!");
        console.log(this.props.passedProps);
        return (
            <div id={this.props.surveyId}>
                {this.renderChildren(props)}
            </div>


        )
    }
});

var QuestionGroup = React.createClass({
    renderChildren: function (passedProps) {
        return(passedProps.map(function(question){
            var AnswerType = passedProps.componentRef[question.type];
            console.log(question);
            console.log(AnswerType);

            console.log(passedProps);
            var props = question;
            props.componentRef = passedProps.componentRef;
              if(AnswerType  !== undefined) {
                  return (
                      <div key = {question.surveyId}>
                          <div>{question.text}</div>
                          <AnswerType {...props} id = {question.surveyId}/>
                      </div>)
              }
            else{
                  return(<div key = {question.surveyId}>
                      {question.text}
                       {question.surveyId}"Undifined!"</div>)
              }
        }));


    },
    render: function () {
        var props = {};
        props = this.props.children;
        props.componentRef = this.props.componentRef;

        console.log("Here's the QuestionGroup!");
        console.log(this.props.passedProps);
        return (
            <div id={this.props.surveyId}>
                {this.renderChildren(props)}
            </div>


        )
    }
});

var QuestionController = React.createClass({
    //require a parameters, an array of objects which can be added to props
    getQuestionProps: function(parameters) {
        var props = {};
        for (var key in parameters){
            props[key] = parameters[key];
        }
        return props;
    },
    renderChildren:function(passedProps){
        return React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, passedProps)
        });
    },
    render:function(){
        var props = this.getQuestionProps(this.props.parameters);
        return(
            <div>
                {this.renderChildren(props)}
            </div>
        )
    }


});

var QuestionView = React.createClass({
    getCSS:function(){
        return(
        {display:'inline-block',
            position:'relative'}
        )
    },
    getChild:function(){
        return(
            //something.
            console.log("welp")
        );
    },
    render:function(){
        return(<div style = {this.getCSS()}>
                <div style = {this.getCSS()}>{this.props.text}</div>
                {React.createElement(TextEntry, this.props, <TextboxView/>)}
            </div>
        )
    }
});

var QuestionComponent = React.createClass({
    render:function(){
        return(
            <QuestionController parameters = {this.props.parameters}f>
                <QuestionView/>
            </QuestionController>

        )
    }
});

React.render(<QuestionComponent parameters = {test_question1}/>,
    document.getElementById("first_question_JSON"));

module.exports.QuestionGroup = QuestionGroup;
module.exports.ConditionalDeclineQuestionGroup = ConditionalDeclineQuestionGroup;