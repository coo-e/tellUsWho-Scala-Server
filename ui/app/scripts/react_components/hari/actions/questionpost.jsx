var Reflux = require('reflux');

/**
 * todo - need to add function for posting answer
 * todo - currently looking at when to POST, componentDidUpdate updates too often
 */
exports.QuestionActions = Reflux.createActions([
    'fetchList',

]);

exports.ResponseActions = Reflux.createActions([
    'updateResponses',
    'postAnswers'
]);

