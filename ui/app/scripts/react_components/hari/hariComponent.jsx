var React = require('react');

//def

var Slider = React.createClass({
    render:function(){
        return (
            <div>
                <input ref="input" type="range" min="0" max="255" onChange = {this.props.update}/>
            </div>
        )
    }
})
module.exports = React.createClass({
    //mixins: [Reflux.connect(store)],
    getInitialState: function(){
       return {
           red:0,
           green:0,
           blue:0
       }
    } ,
    update: function(){
        this.setState({
            red:this.refs.red.refs.input.getDOMNode().value,
            green:this.refs.green.refs.input.getDOMNode().value,
            blue:this.refs.blue.refs.input.getDOMNode().value
        })
    },
    getDefaultProps:function(){
      return {
          number: 0,
          message: "some default prop text"
      }
    },
    //propTypes : {
    //    number: React.PropTypes.isRequired,
    //    message: React.PropTypes.string
    //},
    render: function () {
        return (
            <div>
                <Slider ref="red" update={this.update} />
                <label>{this.state.red}</label>
                <Slider ref="green" update={this.update}  />
                <label>{this.state.green}</label>
                <Slider ref="blue" update={this.update} />
                <label>{this.state.blue}</label>
            </div>
        );
    }
});
