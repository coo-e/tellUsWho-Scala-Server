
var mui=require('material-ui');
var RaisedButton=mui.RaisedButton;

var Button = React.createClass({
    getInitialState:function(){
        return {val:0}
    },
    update:function(){
        this.setState(
            {val: this.state.val+1}
        );
    },
    componentWillMount:function(){
      this.setState({m:2})
    },
    componentDidMount: function(){
      this.inc = setInterval(this.update,500)
    },
    componentWillUnmount:function(){
        console.log(this.getDOMNode());
        clearInterval(this.inc);
    },
    render:function(){
        console.log('rendering');
        return <a className="btn btn-primary" onClick={this.update}>{this.state.val*this.state.m}</a>
    }
});

var MountCtrl = React.createClass({
    mount:function(){
        React.render(<Button />, document.getElementById('mntCtrl'));
    },
    unMount:function(){
        React.unmountComponentAtNode(document.getElementById('mntCtrl'));
    },
    render:function(){
        return(
            <div className="btn-group" role="group">
                <RaisedButton label="wtf" />
                <a onClick={this.mount} className="btn btn-default">Mount</a>
                <a onClick={this.unMount} className="btn btn-default">Unmount</a>
                <div id="mntCtrl"></div>
            </div>
        )
    }
});
//React.render(<MountCtrl/>, document.getElementById('inc'));
///module.exports=MountCtrl;