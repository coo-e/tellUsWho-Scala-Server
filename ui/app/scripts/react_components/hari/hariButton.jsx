var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var bsButton = ReactBootstrap.Button(
    {
        bsStyle:"primary",
        bsSize:"large"
    }
);

var Button = React.createClass({
    render:function(){
        return <a className="btn btn-primary btn-large">{this.props.children}</a>
    }
});

var icons = ReactBootstrap.Glyphicon;

var oneHeart = (
  <icons glyph='heart' />
);
var Heart = React.createClass({
    render:function(){
        return <span className="glyphicon glyphicon-heart"></span>
    }
});
var App = React.createClass({
    render: function(){
        return <Button> I <Heart /> React </Button>
    }
});

React.render(<App />, document.getElementById('heart'))