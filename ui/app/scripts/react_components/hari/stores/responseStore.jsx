var Reflux = require('reflux');
var request = require('superagent');
var React = require('react')
var qActs = require('../actions/questionpost');
var qStore = require('./questionStore');

var ResponseStore = Reflux.createStore({
    listenables: [qActs.ResponseActions],
    responseList: [],
    //mixins:[Reflux.connect(QuestionStore,'questionStore')],
    postUrl: "http://localhost:9000/response/save/harirao3@gmail.com",
    init:function(){
        this.listenTo(qStore,this.onQuestionsFetched);
    },
    onQuestionsFetched: function(data){
        data.map(function(question){
             var responseJSON={
                "useremail":"harirao3@gmail.com",
                "questionid":question.id,
                "id":question.id,
                "response":""}
            ResponseStore.responseList.push(responseJSON)
        })
        console.log(ResponseStore.responseList)
    },
    updateResponses:function(data){
        //this.responseList.responseJSON
    },
    postAnswers : function(data){
        request
            .post(this.postUrl)
            .send(data)
            .end(function(err,res){
                console.log(res)
            })
    }
})

module.exports = ResponseStore;