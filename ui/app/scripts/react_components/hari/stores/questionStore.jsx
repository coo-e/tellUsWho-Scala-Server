var Reflux = require('reflux');
var request = require('superagent');
var qActs = require('../actions/questionpost');
//var wtf = require(this);

var QuestionStore = Reflux.createStore({
    listenables: [qActs.QuestionActions],
    questionlist: [],

    interestUrl: 'http://telluswho.connectionslab.net:9000/questions/interest',
    backgroundUrl: 'http://telluswho.connectionslab.net:9000/questions/background',

    init: function() {
        this.fetchList();
    },
    fetchList: function() {
        request
            .get(this.interestUrl)
            .end(function(err,res){
                console.log(res.body);
                QuestionStore.questionlist=res.body;
                QuestionStore.trigger(QuestionStore.questionlist)
            })
    }
});

module.exports = QuestionStore