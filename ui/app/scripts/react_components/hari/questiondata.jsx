var React = require('react');
var request = require('superagent');
var Reflux = require('reflux');
var ResponseStore = require('./stores/responseStore');
var QuestionStore = require('./stores/questionStore');
var finalStore = require('./stores/finalResponseStore');
var $ = require('jquery');

/*<!--<input type="text" value={message} onChange= {this.handleChange} />-->*/

var Answer = React.createClass({
    mixins: [Reflux.connect(ResponseStore,"responseStore")],
    getInitialState: function() {
        return {value: 'Hello!'};
    },
    handleChange: function(event) {
        this.setState({value: event.target.value});
        console.log(event.target.value)
    },
    componentDidUpdate: function(){
        console.log(event.target.value)
       ResponseStore.trigger(ResponseStore.updateResponses(event.target.value))
    },
    render: function() {
        var value = this.state.value;
        return <input type="text" value={value} onChange={this.handleChange} />;
    }
});


var QuestionGrid = React.createClass({
    mixins:[Reflux.connect(QuestionStore,'questionStore')],
    render:function() {
        if(this.state.questionStore){
            return (
                <table>
                <th><td>Question</td><td>Answer</td></th>
                    {this.state.questionStore.map(function(question){
                        return (
                            <tr>
                                <td>{question.question}</td>
                                <td ><Answer /></td>
                            </tr>
                        )
                    })}
                </table>
            )
        }
        else {return (
            <table>
            </table>
        )}
    }
})

var Form = React.createClass({
    render:function(){
        return(
            <div>
                <QuestionGrid />
                <a href="http://telluswho.connectionslab.net:9000/questions/background">Background questions</a>
            </div>
        )
    }
})

React.render(<Form />, document.getElementById('QuestionForm'))