var React =require('react');
var _ = require('lodash');
var Select = require('react-select');



var componentRef = require('../../fakedata/ComponentArray').componentRef;
var test_survey= require('../mock_front_json/mock_json_test').test_survey;

function ResponseListener(ID, Response){
    console.log("Here's the ID: " + id + " and here's the response" + response)
}


var SurveyController = React.createClass({
    getInitialState:function(){
        return({
            currSection:0,
            currSubSection:0,
            currPage:0})

    },
    SurveyData:function(temp){

        var props = {};
        for (var key in temp) {
                if (temp.hasOwnProperty(key)){
                    props[key] = temp[key];
                    }
        }
        return(props);

    },
    nextPage:function(){
        var nextPage = this.state.currPage + 1;
        this.setState({currPage:nextPage})
    },
    renderChildren:function(passedProps){
        var MySubSections = passedProps.children;
        var currSubSection = this.state.currSubSection;

        var props ={};
        props.passedProps = MySubSections[currSubSection];
        props.currPage = this.state.currPage;
        props.componentRef = this.props.componentRef;
        var MyChild = componentRef[MySubSections[currSubSection].type];
        return(
       <MyChild {...props}/>

        )
    },
    render: function(){
        var props = this.SurveyData(this.props.test_survey);


        return(
            <div id = {props.surveyId}>
                {this.renderChildren(props)}
            </div>
        );
    }
});

React.render(<SurveyController test_survey = {test_survey} componentRef = {componentRef}/>, document.getElementById('pageTest'));