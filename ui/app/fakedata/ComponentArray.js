/**
 * Created by Jakeland on 8/30/2015.
 */
var React = require('react');
var _ = require('lodash');
var Select = require('react-select');

var SubSection = require('../scripts/react_components/SubSectionComponent').SubSection;
var QuestionGroup = require('../scripts/react_components/question').QuestionGroup;
var Page = require('../scripts/react_components/PageComponent').Page;
var ConditionalDeclineQuestionGroup = require('../scripts/react_components/question').ConditionalDeclineQuestionGroup;
var test_survey= require('../scripts/mock_front_json/mock_json_test').test_survey;
var DropDown = require('../scripts/react_components/answerComponent').DropDown;
var VerticalLayout = require('../scripts/react_components/textEntry').VerticalLayout;
var HorizontalLayout = require('../scripts/react_components/layout').HorizontalLayout;
var TextEntry = require('../scripts/react_components/answerComponent').TextEntry;
var ComboSearchEntry = require('../scripts/react_components/answerComponent').ComboSearchEntry;


var componentRef = {};
/*componentRef["Survey"] = Survey;*/
/*componentRef["FilteredMultiTextEntry"] = FilteredMultiTextEntry;*/
/*componentRef["FilteredSingleTextEntry"] = FilteredSingleTextEntry*/
/*componentRef["S*/
componentRef["SubSection"] = SubSection;
/*componentRef["TextArea"] = TextArea;*/
componentRef["ConditionalDeclineQuestionGroup"] = ConditionalDeclineQuestionGroup;
componentRef["ComboSearchEntry"] = ComboSearchEntry;
componentRef["Page"] = Page;
componentRef["QuestionGroup"] = QuestionGroup;
componentRef["DropDown"] = DropDown;
componentRef["Horizontal"] = HorizontalLayout;
componentRef["Vertical"] = VerticalLayout;
componentRef["TextEntry"] = TextEntry;
/*componentRef["FilteredMultiTextEntry"] = FilteredMultiTextEntry;*/
console.log(componentRef["SubSection"]);

module.exports.componentRef = componentRef;